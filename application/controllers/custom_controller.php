<?php

class custom_controller extends CI_Controller
{
    public function index()
    {
        echo "Custom controller reached please leave a message";
    }

    public function search()
    {
        $search_word = $this->input->post('search_word');

        $search_result = $this->opportunity_model->search($search_word);


        $data = array(
            'success' => TRUE,
            'matches_found' => $search_result,
        );

        echo json_encode($data);

//        $this->load->view('search_results');
    }

    public function addToDatabase()
    {

        /*validate form*/
        $this->form_validation->set_rules('company', 'Company', ['trim', 'required', 'min_length[2]', 'max_length[60]']);
        $this->form_validation->set_rules('opp_level', 'Opportunity Level', ['trim', 'required', 'min_length[2]', 'max_length[60]']);
        $this->form_validation->set_rules('opp_description', 'Opportunity Description', ['trim', 'required', 'min_length[2]', 'max_length[300]']);
        $this->form_validation->set_rules('opp_date', 'Opportunity Due Date', ['trim', 'required', 'min_length[2]', 'max_length[20]']);
        $this->form_validation->set_rules('location', 'Company Location', ['trim', 'required', 'min_length[2]', 'max_length[20]']);

        /*get data to the database through a model*/
        /*print out errors*/
        if ($this->form_validation->run() == FALSE) {
            $string_errors = validation_errors() . "";
            $data = array(
                'success' => FALSE,
                'errors' => $string_errors
            );
            /*$this->session->set_flashdata($data);
            redirect('welcome/nav');
            echo 'Failed';*/

            echo json_encode($data);
        } else {
            /*correct data has been input*/
            $company = $this->input->post('company');
            $opportunity_level = $this->input->post('opp_level');
            $opportunity_description = $this->input->post('opp_description');
            $opportunity_date_due = $this->input->post('opp_date');
            $company_location = $this->input->post('location');

            $data_to_send = [
                'company' => $company,
                'opportunity_level' => $opportunity_level,
                'opportunity_description' => $opportunity_description,
                'opportunity_date_due' => $opportunity_date_due,
                'location' => $company_location
            ];

            $result = $this->opportunity_model->add_to_database($data_to_send);

            $data = array(
                'success' => TRUE,
                'result' => $result
            );
            echo json_encode($data);

        }/*end if */


    }  /*end add to database*/


    public function get_all_opportunities()
    {
        $opps = $this->opportunity_model->display_opportunities();
        $this->load->view('navigation_tabs', $opps);
    }

    public function get_opp_desc()
    {
        $company_name = $this->input->post('company_name');
        $opp_level = $this->input->post('opp_level');

        $obj['trial'] = $this->opportunity_model->get_desc($company_name, $opp_level);

        foreach ($obj['trial'] as $item) {
            $desc = $item->opportunity_description;
        }

        $data = array(
            'success' => TRUE,
            'desc' => $desc
        );

        echo json_encode($data);
    } /*function ends here*/


    /*get the desc*/
    public function desc()
    {
        $here['trial'] = $this->opportunity_model->get_desc();

        foreach ($here['trial'] as $item) {
            echo $item->opportunity_description;
        }
    }

    public function delete_event()
    {
        $event_name = $this->input->post('event_name');
        $return = $this->opportunity_model->delete_event($event_name);

        $data = array(
            'success' => TRUE,
            'delete_' => $return
        );

        echo json_encode($data);
    }

    public function delete_opp()
    {
        $opp_name = $this->input->post('opp_name');
        $company_name = $this->input->post('company_name');

        $return = $this->opportunity_model->delete_opp($opp_name, $company_name);

        $data = array(
            'success' => TRUE,
            'delete_' => $return
        );

        echo json_encode($data);
    }


    /*create event in the database*/
    public function create_event()
    {

        /*validate form*/
        $this->form_validation->set_rules('event_name', 'Event Name', ['trim', 'required', 'min_length[2]', 'max_length[60]']);
        $this->form_validation->set_rules('event_location', 'Event Location', ['trim', 'required', 'min_length[2]', 'max_length[60]']);
        $this->form_validation->set_rules('event_desc', 'Event Description', ['trim', 'required', 'min_length[2]', 'max_length[300]']);
        $this->form_validation->set_rules('event_date', 'Event Date', ['trim', 'required', 'min_length[2]', 'max_length[20]']);


//        redirect('welcome/trylogin');

        /*print out errors*/
        if ($this->form_validation->run() == FALSE) {
            $string_errors = validation_errors() . "";
            $data = array(
                'success' => FALSE,
                'msg' => $string_errors
            );
            echo json_encode($data);
        } else {
            /*process image data*/
            $config['upload_path'] = './assets/images/uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 100;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('event_icon')) {
                $data = array(
                    'success' => FALSE,
                    'msg' => $this->upload->display_errors());
                echo json_encode($data);

            } else {
                /*image is alright*/
                $upload_data = $this->upload->data();
                /*get file name*/

                foreach ($upload_data as $item => $value) {
                    if ($item == 'file_name') {
                        $file_name = $value;
                    }
                }

                /*correct data has been input*/
                $event_name = $this->input->post('event_name');
                $event_desc = $this->input->post('event_desc');
                $event_date = $this->input->post('event_date');
                $event_location = $this->input->post('event_location');
                $event_icon = $file_name;

                $data_to_send = [
                    'event_name' => $event_name,
                    'event_date' => $event_date,
                    'event_desc' => $event_desc,
                    'event_location' => $event_location,
                    'event_logo' => $event_icon
                ];

                $result = $this->opportunity_model->create_event($data_to_send);

                $data = array(
                    'success' => TRUE,
                    'msg' => $result
                );
                echo json_encode($data);
            }

        }

        /*take image and process its errors*/


    }/*end create event*/

    public function refresh_divs()
    {
        $data['events'] = $this->opportunity_model->display_events();

        $this->load->view('notice_board/events', $data);


    }


}