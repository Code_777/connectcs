<?php

require_once 'Welcome.php';
/*
Contains funtions used in editing user details
*/


class EditDetails extends CI_Controller{

	//if ($this->session->userdata('logged_in')) {
	//response to click on edit profile button
	

	//response on uploading a picture and saving it in db
    public function do_upload()
    {
        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1000';
        $config['max_width']  = '10240';
        $config['max_height']  = '10240';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            

            $error = array(
                 
                'err' => $this->upload->display_errors());

            $this->load->view('edit', $error);
        }
        
        else
        {
            //$data = array('upload_data' => $this->upload->data());
            $data= $this->upload->data();
             if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $name= $session_data['name'];
            $pic=array( 
                'avatar'=>$data['file_name']);
            $this->Usermodel->updatePic($name,$pic);
           
            

           
            $this->index();
        }
        }
    }
    //at fisrt the forms are two but planning on joining them
    public function editDetails(){
            $session_data= $this->session->userdata('logged_in');
            $email= $session_data['email'];
 $up= array(
        'name'=>$this->input->post('username'),
        //$this->input->post('phone')
        'interests'=>$this->input->post('inter')
        //$this->input->post('pro')
        );
 if($this->Usermodel->upDetails($email,$up)){
    $this->index();

 }

    }
/*}else
{

	$this->load->view('landtest');
}*/


}