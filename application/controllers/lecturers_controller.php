<?php

/**
 * Created by PhpStorm.
 * User: James
 * Date: 3/10/2016
 * Time: 6:23 AM
 */
class lecturers_controller extends CI_Controller
{

    public function works()
    {
        echo "This works";
    }

    public function addToDatabase()
    {

        /*form validation*/
        $this->form_validation->set_rules('image_file', 'Image_file', ['trim', 'max_length[30]']);
        $this->form_validation->set_rules('name', 'Name', ['trim', 'required', 'min_length[2]', 'max_length[30]']);
        $this->form_validation->set_rules('email_address', 'Email address', ['trim', 'required']);
        $this->form_validation->set_rules('phone_number', 'Phone Number', ['trim', 'required']);
        $this->form_validation->set_rules('field_of_competence', 'Field Of Competence', ['trim', 'required']);

        /*print out any errors in validation*/
        if ($this->form_validation->run() == FALSE) {
            $data = array('errors' => validation_errors());
            /*redirect to home and print out errors*/
            $this->session->set_flashdata($data);
            redirect('nav_tabs');
        } else {
            /*valid data, add to database*/
            $image_file = $this->input->post('image_file');
            $name = $this->input->post('name');
            $email_address = $this->input->post('email_address');
            $field_of_competence = $this->input->post('field_of_competence');
            $phone_number = $this->input->post('phone_number');


            $data = ['image_file' => $image_file,
                'name' => $name,
                'email_address' => $email_address,
                'phone_number' => $phone_number,
                'field_of_competence' => $field_of_competence
            ];

            $this->lecturers_model->insert($data);

        }


    }

}