<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * 
     */

     

    public function redir(){

        redirect('welcome');

    }

    public function index(){

   // $this->load->view('learningjs');
       $this->user();
    }


    public function admin(){


        $this->load->view('adminpanel');
    }
   
   /*public function user(){

    redirect('welcome/userlog');
   }*/


   
    public function user(){



        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $name= $session_data['name'];

            $data= array();
            if(!empty($session_data['reg'])){

                $data['pic']=$this->Usermodel->getPic($name);

            }elseif (!empty($session_data['work'])) {
                # code...
                 $data['userAl']=$this->Usermodel->getAl($name);

            }elseif(!empty($session_data['phone'])){

                 $data['userLek']=$this->Usermodel->getL($name);
            }elseif (!empty($session_data['level'])) {
                # code...
          

                $data['useradd']=$this->Usermodel->getA($name);

            }
            else{
                    $this->logout();
                 
            }

                


        /*welcome 2*/
       // $this->load->model('Usermodel');
       
       
       
        
        $data['attachment']= $this->Usermodel->getAttach();
        $data['alumni']= $this->Usermodel->getAlumni();
        $data['posts']= $this->Usermodel->getPosts();
        $data['replies']= $this->Usermodel->getReplies();
        $data['replies2']= $this->Usermodel->getReplies2();
        $data['students']= $this->Usermodel->getStudents();
         $data['opps2']= $this->Usermodel->getOpps();


        $this->load->model('Opportunity_model');
        $data['events'] = $this->Opportunity_model->display_events();
        $data['opps'] = $this->Opportunity_model->display_opportunities();
        $this->load->view('navigation_tabs', $data);
       // redirect('welcome');
    }
else {
    redirect('welcome/trylogin');

}
}

    public function trylogin(){
        $this->load->view('landtest');
    }

    public function signuprid(){
        redirect('NewUser/signup');
        // $this->load->view('Sign_in');
    }
    
    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]');

        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');   
         
         if($this->form_validation->run() == FALSE)
          {

                $data = array(
                            'err'=>TRUE,

                        'errors' => validation_errors()
                    );
                echo json_encode($data);

                //$this->session->set_flashdata($data);
                //echo 'erorr';
                //$this->load->view('landtest');


            }   

         else {

              $username = $this->input->post('username');
              $password =$this->input->post('password');
              $username=$_POST['username'];
              $password=md5($_POST['password']);
              $username=$this->security->xss_clean($username);
              $password=$this->security->xss_clean($password);
              $student=$this->Usermodel->login($username,$password);

             
              $lek=$this->Usermodel->getUserLek($username,$password);
              $alumni=$this->Usermodel->getUserAlumin($username,$password);
              ///if(md5($password) )
              if($student){
                
                $sess_array = array();
            foreach ($student as $row) {
                $sess_array = array(
                    'id'=>$row->std_id,
                    'name' => $row->UserName,
                    'reg' => $row->regno,
                    'email'=>$row->Email,
                    'interests'=>$row->Interests,
                    'year'=>$row->Year,
                    'pic'=>$row->avatar
                    
                );
                $this->session->set_userdata('logged_in', $sess_array);
               
            }
            $valid= "redir";
                echo json_encode($valid);


                //$this->load->view('navigation_tabs'); 
               // $this->user();

            }
            elseif ($lek) {
                # code...
                $sess_array = array();
            foreach ($lek as $row) {
                $sess_array = array(
                    'id'=>$row->lek_id,
                    'name' => $row->UserName,
                    'phone'=>$row->phone
                    
                );
                $this->session->set_userdata('logged_in', $sess_array);
               
            }
            $valid= "redir";
                echo json_encode($valid);



            }
            elseif ($alumni) {
                # code...

                $sess_array = array();
            foreach ($alumni as $row) {
                $sess_array = array(
                    'id'=>$row->al_id,
                    'name' => $row->UserName,
                    'work'=>$row->work
                    
                );
                $this->session->set_userdata('logged_in', $sess_array);
               
            }
            $valid= "redir";
                echo json_encode($valid);
            }

            else
            {
                //$this->load->view('landtest');
                echo json_encode("failed");
               //echo $password ."   ". $username;
            }


         }
     }


     public function adminlogin(){
            ///$pass="Code777";
             $pass=$_POST['pass'];
             $name=$_POST['user'];
               //$name="Code777";

               $admin=$this->Usermodel->getUserAdmin($name,$pass);
                if ($admin) {
                # code...
                $sess_array = array();
            foreach ($admin as $row) {
                $sess_array = array(
                    'name' => $row->UserName,
                    'level'=>$row->level
                    
                );
                $this->session->set_userdata('logged_in', $sess_array);
               
            }
            $valid= "redir";
                echo json_encode($valid);
            }

             else
            {
                //$this->load->view('landtest');
                echo json_encode("failed");
            }





     }


     public function logout(){
        $this->session->unset_userdata('logged_in');
        session_destroy();
        //redirect('login_view', 'refresh');
        
        redirect('welcome');
    }



    // Editing user details

    public function edit(){
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
           $id= $session_data['id'];
           $std= $this->Usermodel->getStd($id);
           $lek=$this->Usermodel->getLek($id);
           $al=$this->Usermodel->getAlu($id);

           if(isset($session_data['reg']))
           {


           foreach ($std as $ss) {
               # code...
           
        $error = array(
                 
                'err' => "",
                'fname'=>$ss->FirstName,
                'lname'=>$ss->LastName,
                'inter'=>$ss->Interests,
                'pro'=>"",
                'pic'=> $ss->avatar
                );
    }
       // $error['pic']=$this->Usermodel->getPic($email);
        $this->load->view('edit',$error);
    }
     if(isset($session_data['work']))
           {


           foreach ($al as $ss) {
               # code...
           
        $error3 = array(
                 
                'err' => "",
                'user'=>$ss->FirstName,
                'lname'=>$ss->LastName,
                'phone'=>$ss->work,
                'inter'=>$ss->Interests,
                'work'=>$ss->work,
                'phone'=> $ss->phone,
                'pic'=> $ss->avatar
                );
    }
       // $error['pic']=$this->Usermodel->getPic($email);
        $this->load->view('editAlumni',$error3);
    }
     if(isset($session_data['phone']))
           {


           foreach ($lek as $ss) {
               # code...
           
        $error = array(
                 
                'err' => "",
                'user'=>$ss->FirstName,
                'lname'=>$ss->LastName,
                'inter'=>$ss->Interests,
                'pro'=>"",
                'phone'=> $ss->phone,
                'pic'=> $ss->avatar
                );
    }
       // $error['pic']=$this->Usermodel->getPic($email);
        $this->load->view('editLecturer',$error);
    }
}
}

  //response on uploading a picture and saving it in db



//Changing users profile photo
    public function do_upload()
    {
        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1000';
        $config['max_width']  = '10240';
        $config['max_height']  = '10240';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            

            $error = array(
                 
                'err' => $this->upload->display_errors());

            $this->load->view('edit', $error);
        }
        
        else
        {
            //$data = array('upload_data' => $this->upload->data());
            $data= $this->upload->data();
             if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $name= $session_data['name'];
            $pic=array( 
                'avatar'=>$data['file_name']);

            if(isset($session_data['reg']))
            $this->Usermodel->updatePic($name,$pic);

            if(isset($session_data['phone']))
           $this->Usermodel->updatePicLek($name,$pic);
            if(isset($session_data['work']))
              $this->Usermodel->updatePicAl($name,$pic);   

            

           redirect('welcome');
            
        }
        }
    }
    //at fisrt the forms are two but planning on joining them
    public function editDetails(){
            $session_data= $this->session->userdata('logged_in');
            $email= $session_data['name'];

            if(isset($session_data['reg'])){
 $up= array(
        'FirstName'=>$this->input->post('fname'),
        'LastName'=>$this->input->post('lname'),
        //$this->input->post('phone')
        'Interests'=>$this->input->post('inter')
        //$this->input->post('pro')
        );
 if($this->Usermodel->upDetails($email,$up)){
   // $this->index();
    redirect('welcome');

 }

    }

     if(isset($session_data['work'])){
 $up= array(
         'FirstName'=>$this->input->post('fname'),
        'LastName'=>$this->input->post('lname'),
         'phone'=>$this->input->post('phone'),
        'Interests'=>$this->input->post('inter'),
        'work'=>$this->input->post('work')
        );
 if($this->Usermodel->upDetailsAl($email,$up)){
   // $this->index();
    redirect('welcome');

 }

    }
     if(isset($session_data['phone'])){
 $up= array(
        'FirstName'=>$this->input->post('fname'),
        'LastName'=>$this->input->post('lname'),
            'phone'=>$this->input->post('phone'),
        'Interests'=>$this->input->post('inter')
        //$this->input->post('pro')
        );
 if($this->Usermodel->upDetailsLek($email,$up)){
   // $this->index();
    redirect('welcome');

 }

    }





}
        


}
