<?php

class Opportunity_model extends CI_Model
{

    public function add_to_database($data)
    {
        $this->db->insert('opportunity', $data);
        return "Added to database successfully";
    }

    /*get all opportunities into the view*/
    public function display_opportunities()
    {
        $results = $this->db->query("select * from opportunity ORDER BY opportunity_date_due DESC");
        return $results->result();
    }

    /*get all the events*/
    public function display_events()
    {
        $results = $this->db->query('select * from events');
        return $results->result();
    }

    /*return the description for the opportunity*/
    public function get_desc($company_name, $opportunity_level)
    {
        $result = $this->db->query("select opportunity_description from opportunity
      WHERE opportunity_level = '$opportunity_level'
AND company = '$company_name' ");
        return $result->result();

    }


    public function delete_event($event_name)
    {
        $this->db->where('event_name', $event_name);
        $this->db->delete('events');

        return $this->db->last_query();
    }

    public function search($search_word)
    {

//        $this->db->query
        return "I am looking in the database";
    }

    public function delete_opp($opp_name, $company_name)
    {
        $this->db->where('company', $company_name);
        $this->db->where('opportunity_level', $opp_name);
        $this->db->delete('opportunity');

        return $this->db->last_query();
    }

    public function  create_event($data_sent)
    {
        $this->db->insert('events', $data_sent);
        return "Event created successfully";
    }

    /*return all lecturers*/
    public function display_lecturers()
    {
        $results = $this->db->query('select * from lecturers');
        return $results->result();
    }

}

