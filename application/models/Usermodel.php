<?php


Class Usermodel extends CI_Model{
  function login($username, $password) {
      //  $this->db->select('UserName,Email,avatar');
        $this->db->from('students');
        $this->db->where('UserName', $username);
        $this->db->where('Password',$password);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function getPic($name){
      $this->db->select('UserName,avatar,email,status,regno');
      $this->db->from('students');
      $this->db->where('UserName',$name);
      return $this->db->get()->result();
    }

     public function getL($name){
     
      $this->db->from('lecturers');
      $this->db->where('UserName',$name);
      return $this->db->get()->result();
    }
     public function getLek($id){
     
      $this->db->from('lecturers');
      $this->db->where('lek_id',$id);
      return $this->db->get()->result();
    }
     public function getA($name){
     
      $this->db->from('admin');
      $this->db->where('UserName',$name);
      return $this->db->get()->result();
    }
     public function getAl($name){
     
      $this->db->from('alumni');
      $this->db->where('UserName',$name);
      return $this->db->get()->result();
    }
     public function getAlu($id){
     
      $this->db->from('alumni');
      $this->db->where('al_id',$id);
      return $this->db->get()->result();
    }
    public function getUserLek($username,$password){
      
       $this->db->from('lecturers');
        $this->db->where('UserName', $username);
        $this->db->where('Password',$password);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
     public function getUserAdmin($username,$password){
      
      $this->db->where('UserName',$username);
      $this->db->where('Password',$password);
      return $this->db->get('admin')->result();
    }

     public function getUserAlumin($username,$password){
      
       $this->db->from('alumni');
        $this->db->where('UserName', $username);
        $this->db->where('Password',$password);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function getStd($id){
      $this->db->where('std_id',$id);
      $this->db->from('students');
      return $this->db->get()->result();
    }
    public function getPosts(){

      $query = $this->db->query('select * from post order by post_id DESC');

      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
      public function getPost($id){

        $this->db->where('post_id',$id);
        $check=$this->db->get('post');
        if($check){
          return $check->result();
        }else
        return false;
      
    }
     public function getOpps(){

      $query = $this->db->query('select * from opp2 order by opp_id DESC');

      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }

    //update students
    public function upDetails($name,$change){
      $this->db->where('UserName',$name);

      if($this->db->update('students',$change)){

        return true;
      }else
      {
        return false;
      }

    }

    //update lecturers
    public function upDetailsLek($name,$change){
      $this->db->where('UserName',$name);

      if($this->db->update('lecturers',$change)){

        return true;
      }else
      {
        return false;
      }

    }

    //update alumni
     public function upDetailsAl($name,$change){
      $this->db->where('UserName',$name);

      if($this->db->update('alumni',$change)){

        return true;
      }else
      {
        return false;
      }

    }
      /*
      $this->db->from('post');
      $this->db->
       $this->db->limit(5);

        //$this->db->offset($offset);

        $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }*/
public function getReplies(){
      
      $this->db->from('replies');
       //$this->db->limit(5);
        //$this->db->offset($offset);

        $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }

    public function getReply($id){
      
      $this->db->where('r_key',$id);

        $query = $this->db->get('replies');


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
    public function getReplies2(){
      
      $this->db->from('replies2');
       //$this->db->limit(5);
        //$this->db->offset($offset);

        $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
public function getStudents(){
  $this->db->from('students');
 // $this->db->limit(5);

  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
    public function getAlumni(){
  $this->db->from('alumni');
  //$this->db->limit(5);

  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
    public function getAttach(){
  //$this->db->from('attachments');
  //$this->db->limit(5);
      $query = $this->db->query('select * from attachments order by att_id DESC');

  //$query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
public function newPost($post){
    $new= $this->db->insert('post',$post);

   
       if($new){
        return true;
       }else{
        return false;
       }
   }
    
    public function newAttach($attach){
    $new= $this->db->insert('attachments',$attach);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }
     public function newOpp($opp){
    $new= $this->db->insert('opp2',$opp);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }

     public function newreply($reply){
    $new= $this->db->insert('replies',$reply);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }
    public function reply2($reply){
    $new= $this->db->insert('replies2',$reply);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }
    public function delPos($id,$name){
     
    
      $this->db->where('post_id',$id);
     $this->db->where('name',$name);

      $up=$this->db->delete('post');
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function delPosAdmin($id){
     
    
      $this->db->where('post_id',$id);
     

      $up=$this->db->delete('post');
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function delOpp($id,$name){
      $this->db->from('opp2');
    
      $this->db->where('opp_id',$id);
      $this->db->where('by',$name);
      $up=$this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
     public function delOppAdmin($id){
      $this->db->from('opp2');
      $this->db->where('opp_id',$id);
      $up=$this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function delReply($id,$key,$name){

    /*  foreach ($reply as $rep) {
        # code...
        $key= $rep->r_key;
        $id=$rep->reply_id;
        $name=$rep->nama;
      }*/
      $this->db->from('replies');
    
      $this->db->where('r_key',$key);
      $this->db->where('reply_id',$id);
      $this->db->where('name',$name);
      $up= $this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
     public function delReplyp($id){
      $this->db->from('replies');
      $this->db->where('reply_id',$id);

      $up= $this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
     }
         public function delReplyAdmin(){
       $up = $this->db->query('delete from replies');
      //$this->db->where('reply_id',$id);

     // $up= $this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function delReply2($id){
      $this->db->from('replies2');
    
      $this->db->where('reply2_id',$id);
     // $this->db->where('reply2_id',$id);
    //  $this->db->where('name',$name);
      $up= $this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
     public function delReply2p(){
      $up = $this->db->query('delete from replies2');
     // $this->db->from('replies2');
     // $this->db->where('reply2_id',$id);
    //  $this->db->where('name',$name);
     // $up= $this->db->delete('replies2');
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function addUser($add){
    $new= $this->db->insert('students',$add);

  
   }
   public function addAlumni($add){
    $new= $this->db->insert('alumni',$add);

  
   }
   public function addLecturer($add){
    $new= $this->db->insert('lecturers',$add);

  
   }

    public function delAtt($id,$name){
      $this->db->from('attachments');
    
      $this->db->where('att_id',$id);
      $this->db->where('by',$name);
      if($this->db->delete()){
        return true;
      }
    }
     public function delAttAdmin($id){
      $this->db->from('attachments');
    
      $this->db->where('att_id',$id);
      if($this->db->delete()){
        return true;
      }
    }
    public function uplike($key,$data){
      //$this->db->from('replies');
        //$this->db->where('reply_id',$id);
        $this->db->where('r_key',$key);
       $up= $this->db->update('replies',$data);
       if($up){
        return true;
       }else{
        return false;
       }

    }
    public function updatePic($name,$pic){
      //$this->db->from('replies');

        $this->db->where('name',$name);
        $this->db->update('post',$pic);
        $this->db->where('name',$name);
        $this->db->update('replies',$pic);
        $this->db->where('UserName',$name);
       $up= $this->db->update('students',$pic);
       if($up){
        return true;
       }else{
        return false;
       }

    }
     public function updatePicLek($name,$pic){
      //$this->db->from('replies');

        $this->db->where('name',$name);
        $this->db->update('post',$pic);
        $this->db->where('name',$name);
        $this->db->update('replies',$pic);
        $this->db->where('UserName',$name);
       $up= $this->db->update('lecturers',$pic);
       if($up){
        return true;
       }else{
        return false;
       }

    }
     public function updatePicAl($name,$pic){
      //$this->db->from('replies');

        $this->db->where('name',$name);
        $this->db->update('post',$pic);
        $this->db->where('name',$name);
        $this->db->update('replies',$pic);
        $this->db->where('UserName',$name);
       $up= $this->db->update('alumni',$pic);
       if($up){
        return true;
       }else{
        return false;
       }

    }
    function downlike($id,$data){
      //$this->db->from('replies');
        $this->db->where('r_key',$id);
       $up= $this->db->update('replies',$data);
       if($up){
        return true;
       }else{
        return false;
       }
   }
   public function checkEmailStd($email){

    $this->db->from('students');
  //$this->db->limit(5);
    $this->db->where('email',$email);



  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
    
   }
    public function checkEmailLek($email){

    $this->db->from('lecturers');
  //$this->db->limit(5);
    $this->db->where('Email',$email);



  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
    
   }
    public function checkEmailAl($email){

    $this->db->from('alumni');
  //$this->db->limit(5);
    $this->db->where('email',$email);



  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
    
   }
   public function checkUserStd($name){

    $this->db->from('students');
  //$this->db->limit(5);
    $this->db->where('UserName',$name);



  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
    
   }
   public function checkUserLek($name){

    $this->db->from('lecturers');
  //$this->db->limit(5);
    $this->db->where('UserName',$name);



  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
    
   }
    public function checkUserAl($name){

    $this->db->from('alumni');
  //$this->db->limit(5);
    $this->db->where('UserName',$name);



  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
    
   }
    public function checkRegStd($reg){

    $this->db->from('students');
  //$this->db->limit(5);
    $this->db->where('regno',$reg);



  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
    
   }
   public function checkStatus($key,$name){
     $this->db->from('dislikes');

     $this->db->where('UserName',$name);
     $this->db->where('disliked',$key);

     $q= $this->db->get();
     if($q->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
   }
   public function setStatus($data){

//$this->db->from->('likes');
    $this->db->insert('dislikes',$data);
   }

   public function unsetStatus($key,$name){

$this->db->from('dislikes');

     $this->db->where('UserName',$name);
     $this->db->where('disliked',$key);

     $q= $this->db->delete();
   }
    public function checkStatusLikes($key,$name){
     $this->db->from('likes');

     $this->db->where('UserName',$name);
     $this->db->where('liked',$key);

     $q= $this->db->get();
     if($q->num_rows() > 0)
      {
        return true;
      }else{
        return false;
      }
   }
   public function setStatusLikes($data){

//$this->db->from->('likes');
    $this->db->insert('likes',$data);
   }

   public function unsetStatusLikes($key,$name){

$this->db->from('likes');

     $this->db->where('UserName',$name);
     $this->db->where('liked',$key);

     $q= $this->db->delete();
   }




   public function delLikes($id){

    $this->db->from('likes');
    $this->db->where('liked',$id);
    $this->db->delete();
   }
    public function delLikesp(){
      $query = $this->db->query('delete from likes');

   // $this->db->from('likes');
    //$this->db->where('liked',$id);
   // $this->db->delete('likes');
   }
   public function delDislikes($id){

    $this->db->from('dislikes');
    $this->db->where('disliked',$id);
    $this->db->delete();
   }

   public function delDislikesp(){
     $query = $this->db->query('delete from dislikes');

    //$this->db->from('dislikes');
    //$this->db->where('disliked',$id);
   // $this->db->delete('dislikes');
   }
}




?>
