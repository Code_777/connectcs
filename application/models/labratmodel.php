

<?php


Class Usermodel extends CI_Model{
  function login($useremail, $password) {
        $this->db->select('UserName,email,avatar');
        $this->db->from('students');
        $this->db->where('email', $useremail);
        $this->db->where('password', ($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function getPic($email){
      $this->db->select('UserName,avatar,email');
      $this->db->from('students');
      $this->db->where('email',$email);
      return $this->db->get()->result();
    }
    public function getStd($email){
      $this->db->where('email',$email);
      $this->db->from('students');
      return $this->db->get()->result();
    }
    public function getPosts(){

      $query = $this->db->query('select * from post order by post_id DESC');

      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
     public function getOpps(){

      $query = $this->db->query('select * from opp2 order by opp_id DESC');

      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
    public function upDetails($email,$change){
      $this->db->where('email',$email);

      if($this->db->update('students',$change)){

        return true;
      }else
      {
        return false;
      }

    }
      /*
      $this->db->from('post');
      $this->db->
       $this->db->limit(5);

        //$this->db->offset($offset);

        $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }*/
public function getReplies(){
      
      $this->db->from('replies');
       //$this->db->limit(5);
        //$this->db->offset($offset);

        $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
    public function getReplies2(){
      
      $this->db->from('replies2');
       //$this->db->limit(5);
        //$this->db->offset($offset);

        $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
public function getStudents(){
  $this->db->from('students');
  $this->db->limit(5);

  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
    public function getAlumni(){
  $this->db->from('alumni');
  //$this->db->limit(5);

  $query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
    public function getAttach(){
  //$this->db->from('attachments');
  //$this->db->limit(5);
      $query = $this->db->query('select * from attachments order by att_id DESC');

  //$query = $this->db->get();


      if($query->num_rows() > 0)
      {
        return $query->result();
      }else{
        return NULL;
      }
    }
public function newPost($post){
    $new= $this->db->insert('post',$post);

   
       if($new){
        return true;
       }else{
        return false;
       }
   }
    
    public function newAttach($attach){
    $new= $this->db->insert('attachments',$attach);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }
     public function newOpp($opp){
    $new= $this->db->insert('opp2',$opp);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }

     public function newreply($reply){
    $new= $this->db->insert('replies',$reply);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }
    public function reply2($reply){
    $new= $this->db->insert('replies2',$reply);

   
       if($new){
        return true;
       }else{
        return false;
       }
    }
    public function delPos($id){
      $this->db->from('post');
    
      $this->db->where('post_id',$id);
    //  $this->db->where('name',$name);
      $up=$this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function delOpp($id,$name){
      $this->db->from('opp2');
    
      $this->db->where('opp_id',$id);
      //$this->db->where('by',$name);
      $up=$this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function delReply($id){
      $this->db->from('replies');
    
      $this->db->where('reply_id',$id);
      //$this->db->where('time',$name);
      $up= $this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function delReply2($id){
      $this->db->from('replies2');
    
      $this->db->where('r2_key',$id);
      //$this->db->where('time',$name);
      $up= $this->db->delete();
       if($up){
        return true;
       }else{
        return false;
       }
    }
    public function addUser($add){
    $new= $this->db->insert('students',$add);

  
   }

    public function delAtt($id,$name){
      $this->db->from('attachments');
    
      $this->db->where('att_id',$id);
      //$this->db->where('name',$name);
      if($this->db->delete()){
        return true;
      }
    }
    public function uplike($key,$data){
      //$this->db->from('replies');
        //$this->db->where('reply_id',$id);
        $this->db->where('r_key',$key);
       $up= $this->db->update('replies',$data);
       if($up){
        return true;
       }else{
        return false;
       }

    }
    public function updatePic($name,$pic){
      //$this->db->from('replies');

        $this->db->where('name',$name);
        $this->db->update('post',$pic);
        $this->db->where('name',$name);
        $this->db->update('replies',$pic);
        $this->db->where('name',$name);
       $up= $this->db->update('students',$pic);
       if($up){
        return true;
       }else{
        return false;
       }

    }
    function downlike($id,$data){
      //$this->db->from('replies');
        $this->db->where('r_key',$id);
       $up= $this->db->update('replies',$data);
       if($up){
        return true;
       }else{
        return false;
       }
   }
}




?>