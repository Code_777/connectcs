<div id="mySlide" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#mySlide" data-slide-to="0" class="active"></li>
		<li data-target="#mySlide" data-slide-to="1"> </li>
		<li data-target="#mySlide" data-slide-to="2"> </li>
	</ol>

	<div class="carousel-inner" role="listbox">
		<div class="active item">
			<img src="<?php echo base_url()?>assets/images/cc1.jpg" class="img-responsive img-rounded">
			<div class="carousel-caption">
				<h3>Closing the Gap</h3>
				

			</div>
		</div>
		<div class="item">
			<img src="<?php echo base_url()?>assets/images/cc5.jpg" class="img-responsive img-rounded ">
			<div class="carousel-caption">
				<h3>Building unity</h3>
				

			</div>
		</div>
		<div class="item">
			<img src="<?php echo base_url()?>assets/images/cc3.jpg" class="img-responsive img-rounded">
			<div class="carousel-caption">
				<h3>Neutral point</h3>
				

			</div>
		</div>
	</div>

	<a class="left carousel-control " href="#mySlide" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
		</span>
		<span class="sr-only">Previous</span>
	</a>
		<a class="right carousel-control " href="#mySlide" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
		</span>
		<span class="sr-only">Next</span>
	</a>



</div>
