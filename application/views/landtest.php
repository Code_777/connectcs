<?php
 

 if($this->session->userdata('user')){



    $this->session->unset_userdata('user');
 }



?>




<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/forums_style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/nav_style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/latest.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/mycss.css">
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/login_script.js"></script>

</head>
<body class="bcolor">
<div class="container-fluid">

    <div class="row">
        <!--logo -->
        <div class="col-md-7">
            <p style="font-family:'Roboto Thin','Segoe UI Light', Helvetica, Arial, sans-serif;color:#009887 ;font-size: 4em;
    margin-top: 10px">
                <\ConnectCS>
            </p>
        </div>
        <!--empty -->
        <div class="col-lg-2"></div>

        <div class="col-lg-4">
            <div class="row">
                <!--empty -->
                <div class="col-lg-6"></div>
                <!--with buttons -->
                <div class="col-lg-6">
                    <div class="row" style="padding-top: 15%;">
                        <div class="col-md-6">
                            <p></p>

                            <p></p>
                            <a class="btn  active custom_button"
                               href="<?php echo base_url() ?>index.php/NewUser/signup" role="button">Sign Up</a>
                        </div>


                        <div class="col-md-6 ">
                            <p></p>

                            <p></p>
                            <button class="btn custom_button show_login">
                                Log In
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <!--carousel -->
        <div id="land_portion">
            <?php
            $this->load->view('slider3');
            ?>


            <div class="row">
                <footer class="site-footer">
                    <div class="text-center"> @ Copyright ConnectCS</div>
                </footer>

            </div>
        </div>

    </div>


</div>

<!--include login div-->
<?php
$this->load->view('login2') ?>

</body>
</html>
