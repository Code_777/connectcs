		<div class="panel panel-default" id="about-private" style="background-color:transparent">
		    <!-- <div class="panel-heading">
		        <h3 class="panel-title">About Us</h3>
		    </div> -->
		    <div class="panel-body scroll">
		        <div class="row">
		            <div class="col-md-12">
		                <div class="aboutusIntro ">
		                    <p style="font-family: 'Segoe UI Light';color: #fff;">
		                       This a platform for all people who are the major stakeholders of the computer department to unite. 
		                       This people include students being the target for this initiative, the lecturers that walk with them every step of the way and the
		                        alumni who are a great source of motivation as well as information about the real world.
		                        The lecturers and the alumni can post information which will be viewed in the notice board tab.
		                        The students can interact individually with themselves, the lecturers and the alumni. 
		                        They can also post the queries that they got and can get assistance from the lecturers, fellow
		                        students or the alumni.
		                        This is a great source of unity and oneness with the sole goal of exploring this world of
		                        computing.
		                    </p>
		                </div> <!--/about us-->
		            </div>
		        </div>
		        <div class="row">
		            

		            <div class="col-md-6">
		                <div class="panel-group" id="accordion" style="color:black">
		                	
		                    <div class="panel panel-success "><!-- accordion 1-->
		                        <!-- panel-heading -->
		                        <div class="panel-heading"> 
		                            <h4 class="panel-title"> <!-- title 1 -->
		                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionOne">
		                              OUR VISION
		                            </a>
		                           </h4>
		                        </div>
		                        
		                        <div id="accordionOne" class="panel-collapse collapse in">
		                              <div class="panel-body">
		                               To see the computer science department united.
		                              </div>
		                        </div>
		                          </div>
		  
		                    <div class="panel panel-success">  <!-- accordion 2 -->
		                      
		                        <div class="panel-heading"> 
		                          <h4 class="panel-title"> <!-- title 2 -->
		                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionTwo">
		                              OUR MISSION
		                            </a>
		                          </h4>
		                        </div>
		                         <!-- panel body -->
		                        <div id="accordionTwo" class="panel-collapse collapse">
		                          <div class="panel-body">
		                            All systems of education devolved.
		                          </div>
		                        </div>
		                    </div>
		   
		                    <div class="panel panel-success">  <!-- accordion 3 -->
		    
		                        <div class="panel-heading">
		                          <h4 class="panel-title"> <!-- title 3 -->
		                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionThree">
		                              OUR GOAL
		                            </a>
		                          </h4>
		                        </div>
		                        <!-- panel body -->
		                        <div id="accordionThree" class="panel-collapse collapse">
		                           <div class="panel-body">
		                          To see all the departments in all campuses with a one platform for sharing.
		                            </div>
		                        </div>
		                    </div>

		                    <div class="panel panel-success">  <!-- accordion 4 -->
		                      
		                        <div class="panel-heading"> 
		                          <h4 class="panel-title"> <!-- title 4 -->
		                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionFour">
		                              CORE VALUES
		                            </a>
		                          </h4>
		                        </div>
		                        <!-- panel body -->
		                        <div id="accordionFour" class="panel-collapse collapse">
		                          <div class="panel-body">
		                            Teamwork, Intergrity,and Hardwork.
		                          </div>
		                        </div>
		                    </div>
		                </div>
		            </div>   

		            <div class="col-md-6">
		            	
		            
		           				 <div class="panel panel-success">		            
		                
		                				<div class="panel-heading panel-success bac">
		                    					<h4 class="panel-title ">  CONTACT US  </h4>
		                     
		 								</div>
		 								<div class="row">
		            						<div class="col-md-6">

		 								
										 <!-- end of panelbody1 -->
										<div class="panel-body">
											<span>
		                                      <a href="https://www.facebook.com/"> <i class="fa fa-facebook-square " style="font-size:30px;color:teal"></i></a>
		                                     </span>

		                    			</div><!-- end of panel body2 -->
		                    			<div class="panel-body">
		                    				<span>
		                                     <a href="https://www.facebook.com/"> <i class="fa fa-bitbucket-square " style="font-size:30px;color:teal"></i></a>
		                                    </span>

		                    			</div><!-- end of panelbody3 -->
										<div class="panel-body">
											<span>
								 				<a href="https://www.facebook.com/"><i class="fa fa-linkedin-square fa-lg" style="font-size:30px;color:teal"></i></a>
				                			</span>

		                        


										</div><!-- end of panel body 4 -->


<div class="panel-body">
		 								
		 						
		 									<span>
		                                     <a href="https://www.facebook.com/"> <i class="fa fa-twitter-square " style="font-size:30px;color:teal"></i>,</a>
		                                    </span>
		                            </div>



						</div><!-- end of rirst col 3 -->

										
						<div class="col-md-3">
							<div class="panel-body">
											
								 				<a href="https://www.facebook.com/"><i class="fa fa-phone-square fa-lg" style="font-size:30px;color:teal" ></i></a>
				                			
		                   	</div>
		                   	<div class="panel-body">
											<span>
								 				<a href="https://www.facebook.com/"><i class="fa fa-envelope-square fa-lg" style="font-size:30px;color:teal"></i></a>
				                			</span>
		                   	</div>

		                   	<div class="panel-body">
											<span>
								 				<a href="https://www.facebook.com/"><i class="fa fa-google-plus-square fa-lg" style="font-size:30px;color:teal"></i></a>
				                			</span>
		                   	</div>
		                   	<div class="panel-body">
											<span>
								 				<a href="https://www.facebook.com/"><i class="fa fa-youtube-square fa-lg" style="font-size:30px;color:teal"></i></a>
				                			</span>
		                   	</div>



						</div><!-- end of second col-md 3 -->
							</div><!-- end of row -->
		               
		            <!-- end of panel body -->
		            </div> <!-- end o panel panel sucess -->
		             </div><!-- end of second col md 6-->
		            </div>  <!-- end of first row -->
		            </div><!-- end of div panel body post scroll -->

		        </div><!-- end of first class div -->
		    <!-- </div>

		    </div>

		</div>
 -->