<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/x-icon" href="../../assets/images/icons/plus.png"/>
    <title>..::<\ConnectCS>::..</title>

    <!--add the scripts here-->
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/misc/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/posts.js"></script>
    <script src="<?php echo base_url() ?>assets/js/attachmentAndAlumni.js"></script>
    <script src="<?php echo base_url() ?>assets/js/custom_js.js"></script>
    <script src="<?php echo base_url() ?>assets/js/students.js"></script>



    


    <!--styles-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url() ?>assets/misc/jquery-ui-1.11.4.custom/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/notice_board.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/forums_style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/mycss.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/search.css">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/latest.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/font-awesome-4.3.0/css/font-awesome.min.css">
     


</head>
<body class="bcolor">

<!--main container-->


<!--setting the sessions-->

<?php
if(!empty($pic)){
                
                $sess_array = array();
            foreach ($pic as $details) {
                $sess_array = array(
                    'name' => $details->UserName,
                    'reg' => $details->regno,
                    'email'=>$details->email,
                    'status'=>null
                    
                );
                $this->session->set_userdata('user', $sess_array);
}

}elseif (!empty($userLek)) {
    # code...

     $sess_array = array();
            foreach ($userLek as $details) {
                $sess_array = array(
                    'name' => $details->UserName,
                    'level' => null,
                    'email'=>$details->Email,
                    'status'=>"1"
                    
                );
                $this->session->set_userdata('user', $sess_array);
}



}elseif (!empty($useradd)) {
    # code...
     $sess_array = array();
            foreach ($useradd as $details) {
                $sess_array = array(
                    'name' => $details->UserName,
                    'level' => $details->level,
                    'email'=>"Administrator"
                    
                    
                );
                $this->session->set_userdata('user', $sess_array);
}



}elseif (!empty($userAl)) {
    # code...
     $sess_array = array();
            foreach ($userAl as $details) {
                $sess_array = array(
                    'name' => $details->UserName,
                    'level' => null,
                    'email'=>$details->Email,
                    'status'=>$details->status
                    
                );
                $this->session->set_userdata('user', $sess_array);
}

}

                ?>
<div class="container">


    <div class="row">
        <div class="col-md-7">
            <p style="font-family:'Roboto Thin','Segoe UI Light', Helvetica, Arial, sans-serif;color:#009887 ;font-size: 4em;
    margin-top: 10px">
                <\ConnectCS> 
            </p>
        </div>

        <div class="col-md-4" style="padding: 0; width: 20%; float: right; margin: 1% 0 15px;">
            <div class="row">
                <?php $this->load->view('user_details'); ?>
            </div>


            <!--search bar-->
           
        </div>
    </div>


    <!--tabs have been created from here
        styles can be found in the assets folder in css
            named as nav_style.css-->

    <ul class="nav nav-tabs nav-justified" id="my_nav">
        <li class="active" ><a data-toggle="tab" href="#home">Home</a></li>
        <li ><a data-toggle="tab" href="#noticeboard">Notice Board</a></li>
        <li><a data-toggle="tab" href="#attachment">Attachment</a></li>
        <li ><a data-toggle="tab" href="#forum">Forums</a></li>
        <li><a data-toggle="tab" href="#alumni">Alumni</a></li>
        <li><a data-toggle="tab" href="#aboutus">About Us</a></li>
    </ul>

    <!--for each of the tabs the content have been laid out here-->
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
           <?php
            $this->load->view('home');

           ?>
        </div>
        

        <!--Divs for the tabs have already been created,
        all you need to do is Load the view inside your appropriate tab part as shown in the noticeboard tab below
        Kind regards:
            James Gathu-->


        <div id="attachment" class="tab-pane fade">
            <!--content here-->
           
            <?php
            $this->load->view('forum/attachments');
            ?> 
        </div>
         

        

        <div id="alumni" class="tab-pane fade">
            <!--content here-->
            <h4 style="font-family: 'Roboto Thin','Segoe UI Light;color:#009887">
                Welcome To Alumni Page
            </h4>
           <?php
            $this->load->view('forum/alumni');
            ?> 
        </div>

        <div id="aboutus" class="tab-pane fade">
            <?php $this->load->view('about_us'); ?>
        </div>
        <div id="forum" class="tab-pane fade">
            <!--content here-->
            <?php
            $this->load->view('forum/forums_template');
            ?>
        </div>


        <div id="noticeboard" class="tab-pane fade">
            <!--content here-->
             <?php 
    $this->load->view('notice_board/show_lecs'); ?>
                <div id="hidenotice">
            <?php
            $this->load->view('notice_board/panel');
            ?>
            
        </div>
            

<?php $this->load->view('notice_board/forms/opportunity_form'); ?>
<?php 
$this->load->view('notice_board/forms/event_form'); ?>


            

            



        </div>
        



        


    </div>

<div>
   
</div>

    
</div>



<!--add scripts here-->


</body>
</html>