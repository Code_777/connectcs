
	<div class="row"  style="margin-top:27px">
		<div class="col-md-8" >
			<div  class="carousel slide" data-ride="carousel">
				<div id="homecaro" class="carousel-inner item active" style="margin-top:27px">
					
					<img src="<?php echo base_url()?>assets/images/whoweare_final.png" height="250px" width="450px">
					
					
					<div class="carousel-caption">ConnectCs</div>
					
			 	</div>


			 	<video  width="100%" id="add_event" controls style="display: none" >
				  <source src="<?php echo base_url()?>assets/videos/create_event.mp4" type="video/mp4">
				  
				</video> 

				<video id="add_opp" width="100%" controls style="display: none" >
					  <source src="<?php echo base_url()?>assets/videos/add_opp.mp4" type="video/mp4">
					  	
				</video> 

				<video id="add_thread" width="100%" controls style="display: none" >
					  <source src="<?php echo base_url()?>assets/videos/start_thread.mp4" type="video/mp4">
					  
				</video> 
				<video id="find_attachment" width="100%" controls style="display: none" >
					  <source src="<?php echo base_url()?>assets/videos/find_attachment.mp4" type="video/mp4">
					  
				</video>
				<video id="add_attachment" width="100%" controls style="display: none" >
					  <source src="<?php echo base_url()?>assets/videos/add_attachment.mp4" type="video/mp4">
					  
				</video>  
			</div>

			


		</div>
		<div class="col-md-4">

			 <h2 style="font-family: 'Roboto Thin','Segoe UI Light';color:#009887 ;margin-bottom:20px;">
                Getting Started...
            </h2>

			<ul class="list-group" style="color:#009887;border-radius:0px;background-color: rgba(0,0,0,0.50);font-family:'Roboto Thin'">
				  <li class="list-group-item" id="create_event" onclick="show_this(this.id)">Create event</li>
				  <li class="list-group-item" id="create_opp" onclick="show_this(this.id)">Create Opportunity On Notice Board </li>
				  <li class="list-group-item" id="create_thread" onclick="show_this(this.id)">Start a Thread</li>
				  <li class="list-group-item" id="create_attachment" onclick="show_this(this.id)">Create an Attachment</li>
				  <li class="list-group-item" id="find_attachment_" onclick="show_this(this.id)">Find Attachment</li>
			</ul>

			<script type="text/javascript">

			function show_this(video_id){

				/*alert(video_id);*/
				if(video_id == "create_event"){
					$("#add_event").show();
						$("#add_thread").hide();
						$("#add_opp").hide();
						$("#homecaro").hide();
						$("#find_attachment").hide();
						$("#add_attachment").hide();
				}else if(video_id == "create_opp"){
					$("#add_opp").show();
						$("#add_event").hide();
						$("#add_thread").hide();
						$("#homecaro").hide();
						$("#find_attachment").hide();
						$("#add_attachment").hide();
				}else if(video_id == "create_thread"){
					$("#add_thread").show();
						$("#add_opp").hide();
						$("#add_event").hide();
						$("#homecaro").hide();
						$("#find_attachment").hide();
						$("#add_attachment").hide();
				}
				else if(video_id == "create_attachment"){
					$("#find_attachment").show();
						$("#add_opp").hide();
						$("#add_event").hide();
						$("#homecaro").hide();
						$("#add_thread").hide();
						$("#add_attachment").hide();
				}else if(video_id == "find_attachment_"){
					$("#add_attachment").show();
						$("#add_opp").hide();
						$("#add_event").hide();
						$("#homecaro").hide();
						$("#find_attachment").hide();
						$("#add_thread").hide();
				}

						
					

				}
			</script>	

 			<!-- <h2> Create An Event </h2>
 --> 			
			<!-- <div class="video1">
				<a href="#myModal" class="btn btn-lg btn-sucess" data-toggle="modal">Create An Event</a>
				<div id="myModal" class="modal-fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> $times; </button>
									<h2 class="modal-title"> Create an Event</h2>
							</div>
							<div class="modal-body">
								<iframe id="eventVideo" class="embed-responsive-item" src="<?php echo base_url()?>assets/images/Create_an_event.mp4" frameborder="0" allowfullscreen></iframe>
							</div>

						</div>

					</div>
				</div>

			</div>	 -->		
		</div>
	</div>

