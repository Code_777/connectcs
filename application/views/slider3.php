
<div id="mySlide2" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#mySlide2" data-slide-to="0" class="active"></li>
		<li data-target="#mySlide2" data-slide-to="1"> </li>
		<li data-target="#mySlide2" data-slide-to="2"> </li>
	</ol>

	<div class="carousel-inner" role="listbox">
		<div class="active item">
			<div class="col-lg-8">
				<img src="<?php echo base_url()?>assets/images/cc1.jpg" class="img-responsive img-rounded">
				<div class="carousel-caption">
					<h3>Connect Cs</h3>
				</div>
			</div>
			<div class="col-lg-4 well well-lg landslider">
				<p> ConnectCs<br>
				An interactive platform specially created for Egerton Unversity Computer Science Department.  
				  	
			  	</p>
			</div>
		</div>
		<div class="item">
			<div class="col-lg-8">
				<img src="<?php echo base_url()?>assets/images/cc2.jpg" class="img-responsive img-rounded">
				<div class="carousel-caption">
					<h3>Building unity</h3>
				</div>
			</div>
			<div class="col-lg-4 well well-lg landslider">
				<p>Building Unity <br>
				Essentially developed to assist students, lecturers and alumni of the department to familiarize with each other
				at a professional level.
				</p>
			</div>
		</div>
		<div class="item">
			<div class="col-lg-8">
				<img src="<?php echo base_url()?>assets/images/cc3.jpg" class="img-responsive img-rounded">
				<div class="carousel-caption">
					<h3>Neutral Point</h3>
				</div>
			</div>
			<div class="col-lg-4 well well-lg landslider">
				<p>Neutral Point<br>
				We learn, share knowledge and opportunities and bail out each other when necessity arises.
				</p>
			</div>

		</div>
	</div>

	<a class="left carousel-control " href="#mySlide2" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
		</span>
		<span class="sr-only">Previous</span>
	</a>
		<a class="right carousel-control " href="#mySlide2" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
		</span>
		<span class="sr-only">Next</span>
	</a>
	<script type="text/javascript">

		$(function(){

			$('#identifier' ). carousel({
 					interval: 2000 ;
 				 )}
		})

	</script>


</div>



