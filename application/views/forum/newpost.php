<div class="modal fade" id="newpost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header mcolor">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h3 class="modal-title" id="myModalLabel">Make a new Post</h3>
          
         </div>
         <div class="modal-body mcolor">
           
            <div class="feedback">
            </div>
            <?php echo form_open('NewPost/savePost');?>
                   
                   <select name="category" class="form-control" required>
                    <option value="">--Select a Category--</option>
                       <option>Web design</option>
                        <option>Java web and desktop application</option>
                         <option>Computer networking</option>
                          <option>Ethical hacking</option>
                           <option>Android programming</option>
                            <option>IOT and Intel XDK</option>
                             <option></option>
                   </select><br>
                   <textarea type="text" class="form-control" name="newpost" placeholder="Make a new Post" required></textarea>
                    <br>
                    <input id="addpost" type="submit"  class="mbtn" value="Make Post" >
                    <?php echo form_close();?>
 
            <div class="modal-footer">
           <button type="button" class="mbtn" data-dismiss="modal">Close</button>
          
           
       </div>
   </div>
 </div>
</div>
</div>
<div class="modal fade" id="newpost2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h3 class="modal-title" id="myModalLabel">Make a new Post</h3>
          
         </div>
         <div class="modal-body">
           <h1 id="modal-body"</h1>
            <div class="feedback">
            </div>
            <?php echo form_open('NewPost/savePost');?>
                   
                    <input class="form-control"type="text" id="subject" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}" required/>
                   <br>
                   <select name="category" class="form-control" required>
                    <option value="">--Select a Category--</option>
                       <option>Web design</option>
                        <option>Java web and desktop application</option>
                         <option>Computer networking</option>
                          <option>Ethical hacking</option>
                           <option>Android programming</option>
                            <option>IOT and Intel XDK</option>
                             <option></option>
                   </select><br>
                   <textarea type="text" class="form-control" name="newpost" placeholder="Make a new Post" required></textarea>
                    <br>
                    <input id="addpost" type="submit"  class="btn btn-primary" value="Make Post" >
                    <?php echo form_close();?>
 
            <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          
           
       </div>
   </div>
 </div>
</div>
</div>




