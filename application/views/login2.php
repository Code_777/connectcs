<div id="login" class="login_here" style="padding: 3%;">

    <!--some additions-->


    <?php $attributes = array('id' => 'login_form', 'class' => 'form-horizontal '); ?>
    <?php if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;
    ?>

    <?php
    echo form_open('login', $attributes); ?>
    <div class="form-group">


        <?php

        $label = array(
            'style' => "font-family:'Roboto Thin';font-size: 1.3em;color: white"
        );

        $head = array(
            'style' => "font-family: 'Roboto Thin';font-size: 2em;color: white"
        );


        echo form_label("Log In", 'connectcs', $head);

        ?>

        <!--option for close-->
        <a class="close show_login" style="margin-top:10px;color: white;opacity:1" href="#" aria-label="close">×</a>

        <?php
        echo "<br>";


        echo form_label('Username:', 'user_name', $label);

        $data = array(
            'class' => 'form-control custom_input',
            'name' => 'username',
            'id' => 'username',
            'placeholder' => 'Enter Your Username',
            'required' => ''
        );


        ?>
        <?php echo form_input($data); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Password', 'pass_word', $label); ?>
        <?php
        $data = array(
            'class' => 'form-control custom_input',
            'required' => '',
            'name' => 'password',
            'id' => 'password',
            'placeholder' => 'Enter Password'
        );
        ?>
        <?php echo form_password($data); ?>
    </div>

    <div class="row">
        <div class="row alert alert-danger alert-dismissible" id="failed"
             style="display: none">
            <p>
                Invalid password or username</p>
        </div>

        
        <div class="col-md-1 form-group" style="padding-top: 10px;float: right">
            <?php
            $data = array(
                'class' => 'btn custom_inverse_button  ',
                'name' => 'submit',
                'value' => 'Login',
                'style' => "float:right",
            ); ?>
            <?php echo form_submit($data); ?>
        </div>
    </div>

</div>


<?php echo form_close();

?>
<script>
   
</script>
</div>