  <?php $attributes = array('id'=>'signup_form', 'class'=>'form-horizontal '); ?>



  <?php if($this->session->flashdata('errors')): 

   echo $this->session->flashdata('errors');


   endif ;

  ?>


             

  <?php


  echo form_open('NewUser/signupVeri' , $attributes); ?>


  	 <!-- firstname -->

     <div class="form-group">


     	<?php echo form_label('Firstname'); ?>
     		<?php 

     			$data = array(
     				'class'=> 'form-control',
     				'name'=> 'firstname',
            'id'=> 'firstname',
     				'placeholder'=> 'Enter Firstname',
            'required'=>'required'
           // 'value'=>echo $Firstname  			
            	);


     		 ?>



     	<?php echo form_input($data); ?>  



    	 </div>

      <!--  lastname -->
          <div class="form-group">


      <?php echo form_label('Lastname'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'lastname',
             'id'=> 'lastname',
            'placeholder'=> 'Enter Lastname',
            'required'=>'required'
          //  'value'=>$Lastname
            );


         ?>



      <?php echo form_input($data); ?>  



       </div>
       <!-- username -->
       <div class="form-group">


      <?php echo form_label('Username'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'username',
            'id'=> 'username',
            'placeholder'=> 'Enter Username',
            'required'=>'required'
  //'value'=>$Username
            );


         ?>



      <?php echo form_input($data); ?>  



       </div>
   <!-- email -->
        <div class="form-group">


      <?php echo form_label('Email'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'email',
            'id'=> 'email',
            'type'=>'email',
            'placeholder'=> 'Enter Email Address',
            'required'=>'required'

            );


         ?>



      <?php echo form_input($data); ?>  



       </div>

      <!--  the radios -->

       <div class="form-group btn-group buttons-radio ">


       <button type="button" name="level" onclick="alumni()">Alumni</button>
       <button type="button" name="level" onclick="lecturers()">Lecturers</button>
       <button type="button" name="level"  onclick="students()">Students</button>

       </div>
       

    <!-- Registration number  and year of study-->
    	 <div  id="stud" class="form-group" style="display:block">


     	<?php echo form_label('Registration Number'); ?>
     		<?php 

     			$data = array(
     				'class'=> 'form-control',
     				'name'=> 'registration',
            'id'=> 'registration',
     				'placeholder'=> 'Enter Registration Number'
     				);


     		 ?>

     	<?php echo form_input($data); ?> 

       
       <br>   
       
    
        <?php echo form_label('Year Of Study'); ?>
        <?php 
       
          $data = array(
            
            ''=>'---',
            
            '1' => 'YEAR 1',
            '2' => 'YEAR 2',
            '3' => 'YEAR 3',
            '4' => 'YEAR 4',
              
              );
       
       
         ?>
       
             <?php echo form_dropdown('year',$data,'','id="year"'); ?>  

            <!--  <select name="year" id="year">
            <option value=""</option>
            <option value="1">YEAR 1</option>
            <option value="2">YEAR 2</option>
            <option value="3">YEAR 3</option>
            <option value="4">YEAR 4</option>
            </select> -->

       </div>
           


       <!-- interests -->
      <div class="form-group">


      <?php echo form_label('Interests'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'interests',
            'id'=> 'interests',
            'placeholder'=> 'Your Interests'
            );


         ?>



      <?php echo form_input($data); ?>  



      </div>
       
       <!-- official phone number -->
       <div id="phoned" class="form-group"  style="display:none">


      <?php echo form_label('Official Phonenumber'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'phone',
             'id'=> 'phone',
            'placeholder'=> 'Your Official PhoneNumber'
            );


         ?>



      <?php echo form_input($data); ?>  



       </div>


        

   <!-- current work place -->
       <div id="workd"class="form-group" style="display:none">


      <?php echo form_label('Current Workplace'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'currentworkplace',
            'id'=> 'currentworkplace',
            'placeholder'=> 'Your Current Work Place'
            );


         ?>



      <?php echo form_input($data); ?>  



       </div>
       <script>
       
       
       </script>
       <!-- password -->
    	 <div class="form-group">


      <?php echo form_label('Password'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'password',
            'id'=> 'password',
            'placeholder'=> 'Enter Password',
            'required'=>'required'
            );


         ?>



      <?php echo form_password($data); ?>  



       </div>
       <!-- confirm password -->
       <div class="form-group">


      <?php echo form_label('Confirm Password'); ?>
        <?php 

          $data = array(
            'class'=> 'form-control',
            'name'=> 'confirm_password',
            'id'=> 'confirm_password',
            'placeholder'=> 'Confirm Password',
            'required'=>'required'
            );


         ?>



      <?php echo form_password($data); ?>  



       </div>
       <div class="alert alert-danger alert-dismissible" id="failed" style="display: none">

                                
                                 </div>

        <div class="alert alert-success alert-dismissible" id="ok" style="display: none">

                                
                                 </div>

        <!-- submit button -->
    	 <div class="form-group" class="col-md-4">


     	   		<?php 

     			$data = array(
     				'class'=> 'mbtn ',
     				'name'=> 'submit',
              'onclick'=>'newuser()',
     				'value'=> 'Sign Up'
     				);


     		 ?> 



     	<?php echo form_submit($data); ?>  
      <div class="col-md-8 pull-right"> Already have an account? 
    <a  class="mbtn" href="<?php echo base_url("index.php/welcome");?>">Login</a>

      </div>



    	 </div>





  <?php echo form_close();
   
   ?>
   
       <script>

    </script>