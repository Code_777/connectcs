<div id="lecturers_modal" class="modal custom fade " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p class="heading1">Lecturers | Details</p>

                <div class="row">
                    <div class="col-md-5 lecturer_image">
                        <!--image goes here-->
                        <img class="lecturer_image" src="<?php echo base_url() ?>/assets/images/avatar.png">
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6" style="font-family: 'Roboto Thin'">
                        <!--details in here-->
                        <p class="lecturer_name"></p>Thor Ordinson <br>

                        <p class="lecturer_email_address">johnteKamauz@egerton.ac.ke</p> <br>

                        <p class="lecturer_phone_number">0724030847</p>

                        <p class="field_of_competence">Java</p>
                    </div>
                </div>

                <?php $this->load->view('hr') ?>
                <?php $this->load->view('lecturer'); ?>
                <?php $this->load->view('hr') ?>

                <!--button after all lecturers-->
                <button class="btn btn-primary cutom_btn" data-dismiss="modal">Close</button>


            </div> <!--end modal body-->
        </div> <!--end modal content-->
    </div> <!--end modal dialog-->
</div> <!--end modal 2-->