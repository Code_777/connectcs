<!-- show errors-->
<div class="jsErrorOrSuccess">
    <div class="alert alert-success alert-dismissible" id="reset_success" style="display: none"
         role="alert">
        <button type="button" class="close hidden" data-dismiss="alert"><span
                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <span id="reset_message"></span>
    </div>
    <div class="alert alert-danger alert-dismissible" id="reset_error" style="display: none">
        <button type="button" class="close hidden" data-dismiss="alert"><span
                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <span id="reset_errormessage"></span>
    </div>

</div> <!--end js error-->