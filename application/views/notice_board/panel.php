<!--pick this as an example to guide you
after creating you content in the manner below
include it in the navigation tabs at the proper place
Use this as an example
Regards:
    James Gathu -->


<div id="leftdiv" class="col-md-6">
    <div class="row">
        <div class="col-md-10">
            <p class="heading1">
                Upcoming Events
            </p> <!--.heading 1-->
        </div>

        

        <div class="col-md-2" style="float: right;padding:20px">
             <?php
        $session_data=$this->session->userdata('user');
      //  echo $session_data['status'];
        if(isset($session_data['status']) | isset($session_data['level']))
        {
            echo'
            <div class="icon">
                <img  class="fab_final add_event"
                     src="'.base_url().'assets/images/icons/plus.png">
                
            </div>

            ';
        }
        ?>
        </div>
    </div>

    <div id="mother" style="overflow-y: auto;overflow-x: hidden; max-height: 320px">
        <!--events-->
        <?php $this->load->view('notice_board/events'); ?>
    </div>

</div> <!--/right div-->

<div class="col-md-1" style="width: 2%">
    <!--space between the right and left div-->
</div> <!--line between divs-->

<!--Right div-->
<div id="rightdiv" class="col-md-5">


    <div class="row">
        <div class="col-md-10">
            <p class="heading1">
                Available Opportunities
            </p> <!--.heading 1-->
        </div>
         <?php
        $session_data=$this->session->userdata('user');
        if(isset($session_data['status']) | isset($session_data['level']))
        {
            echo'
        <div class="col-md-2">
            <div class="icon">
                <img data-toggle="modal" class="fab_final add_opp"
                     src="'.base_url().'assets/images/icons/plus.png">
                
            </div>
        </div>
        ';
    }
    ?>


    </div>

    <div style="overflow-y: auto;overflow-x: hidden; max-height: 320px">
        <!--opportunities-->
        <?php $this->load->view('notice_board/opps') ?>
    </div>


    <?php if ($this->session->flashdata('errors')) {
        echo "<p class='danger' style='color:red;font-family: 'Roboto Thin' ' >";
        echo $this->session->flashdata('errors');
        echo "</p>";
    } elseif ($this->session->flashdata('status')) {
        echo $this->session->flashdata('status');
    }
    ?>

</div>  <!--end right div-->


<!--horizontal line-->
<div class="row">
    <div class="col-md-12">
        <hr style="background-color: #009887; height: 1px; border:none">
    </div>
</div>  <!--end hr-->

<!--find and show the lecturers-->

<div class="row">
    <div class="col-md-2">
        <button class="btn show_lecs" style="background-color: rgb(0,152,135);color: white;font-family: 'Roboto Thin';
        font-size: 1.3em;-webkit-border-radius:0px ;-moz-border-radius:0px ;border-radius: 0px;">Lecturer
            Info
        </button>
    </div>
</div>




