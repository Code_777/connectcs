<!--
event_name
event_logo
event_date-->


<?php


foreach($events as $event) {

    $icon_url = base_url() . "/assets/images/" . $event->event_logo;
    $event_name = $event->event_name;
    $event_date = $event->event_date;


    /*space*/
    echo "<div class='row event'>
    <div class='col-md-1'>
        <!--just some space-->
    </div>";

    /*icons*/
    echo "<div class='col-md-1'> <!--icon-->
    <img src='$icon_url'>
</div>";

    /*date and company name*/
    echo "<div class='col-md-8'>
    <p class='event_holder_name'>
        $event_name
    </p>

    <p class='event_date'>
        $event_date
    </p>
</div><!-- title to event and event date -->
</div>";
} /*close foreach loop*/

?>