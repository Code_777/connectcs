<div class="show_lecturers" id="lec_d">
    <!--header-->
    <div class="row">
        <div class="col-md-11">
            <p class="heading1" style="color: white;">Lecturers Info</p>
        </div>
        <div class="col-md-1">
            <a class="close show_lecs" style="margin-top:20px;color: white;opacity:1" href="#"
               aria-label="close">&times;</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 back_white">
            <div class="col-md-2" style="">
                <!--image goes here-->
                <img src="<?php echo base_url() ?>/assets/images/avatar.png" style="background: 50% 50% no-repeat;
                    width: 50px;
                    height: 50px;
                    border-radius: 50%">
            </div>
            <div class="col-md-10">
                <!--show information-->
                <!--name-->
                <p class="lec_details"><span style="font-weight: 600;">Name:</span> Thor Ordinson</p>

                <p class="lec_details"><span style="font-weight: 600;">Email Address:</span> thorOrdinson@allfather.com
                </p>

                <p class="lec_details"><span style="font-weight: 600;">Official Phone Number:</span> +789 152 562</span>
                </p>
            </div>

            <div class="col-md-1">
                <!--leave some space-->
            </div>
            <hr style="background-color: white">
        </div> <!--end single lecturer-->


        <div class="col-md-4 back_white">

            <div class="col-md-2" style="">
                <!--image goes here-->
                <img src="<?php echo base_url() ?>/assets/images/joy.png" style="background: 50% 50% no-repeat;
                    width: 50px;
                    height: 50px;
                    border-radius: 50%">
            </div>
            <div class="col-md-10">
                <!--show information-->
                <!--name-->
                <p class="lec_details"><span style="font-weight: 600;">Name:</span> Jameson</p>

                <p class="lec_details"><span style="font-weight: 600;">Email Address:</span> jameson@allfather.com</p>

                <p class="lec_details"><span style="font-weight: 600;">Official Phone Number:</span> +789 152 562</span>
                </p>
            </div>

            <div class="col-md-1">
                <!--leave some space-->
            </div>
            <hr style="background-color: white">
        </div> <!--end single lecturer-->

        <div class="col-md-4 back_white">

            <div class="col-md-2" style="">
                <!--image goes here-->
                <img src="<?php echo base_url() ?>assets/images/joy.png" style="background: 50% 50% no-repeat;
                    width: 50px;
                    height: 50px;
                    border-radius: 50%">
            </div>
            <div class="col-md-10">
                <!--show information-->
                <!--name-->
                <p class="lec_details"><span style="font-weight: 600;">Name:</span> Mwangi Njoroge</p>

                <p class="lec_details"><span style="font-weight: 600;">Email Address:</span> mwanginjoro@allfather.com
                </p>

                <p class="lec_details"><span style="font-weight: 600;">Official Phone Number:</span> +789 152 562</span>
                </p>
            </div>

            <div class="col-md-1">
                <!--leave some space-->
            </div>
            <hr style="background-color: white">
        </div> <!--end single lecturer-->


    </div>


</div>