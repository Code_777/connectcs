<div id="form_event" class="form_mine">
    <!--show the errors here-->
    <!-- show errors-->
    <?php
    $attr_event_icon = array(
        'id' => 'event_icon',
        'name' => 'event_icon',
        'type' => 'file',
        'required' => ' ');

    $attr_event_name = array('class' => 'form-control input-sm',
        'id' => 'event_name',
        'name' => 'event_name',
        'placeholder' => 'eg Intel IoT Boot Camp',
        'type' => 'text',
        'required' => ' ');

    $attr_event_desc = array(
        'id' => 'event_desc',
        'class' => 'form-control input-sm',
        'name' => 'event_desc',
        'type' => 'text',
        'placeholder' => 'eg The event is about creation of new technologies',
        'required' => ' ',
        'cols' => '60',
        'rows' => '3');

    $attr_event_date = array(
        'id' => 'event_date',
        'class' => 'form-control input-sm',
        'name' => 'event_date',
        'placeholder' => 'eg 2016-05-21',
        'type' => 'text',
        'required' => ' '
    );

    $attr_event_location = array(
        'class' => 'form-control input-sm',
        'name' => 'event_location',
        'id' => 'event_location',
        'placeholder' => 'eg Egerton University Computer Science Lab 2',
        'type' => 'text',
        'required' => ' '
    );

    $attr_btn_save_event = array(
        'id' => 'btn_save_event',
        'class' => 'btn custom_btn',
        'name' => 'button',
        'value' => 'true',
        'type' => 'submit',
        'content' => 'Save'
    );

    $break_line = "<br>";

    $attr_btn_close = array(
        'class' => "btn custom_btn add_event",
        'name' => 'button',
        'value' => 'true',
        'content' => 'Close',
    );

    $label = array(
        'style' => "font-family:'Roboto Thin';font-size: 1.2em;color: white"
    );

    $head = array(
        'style' => "font-family: 'Roboto Thin';font-size: 2em;color: white"
    );


    echo "<form id='form_e'>";

    echo form_label("Event Details", 'Event details', $head);

    echo $break_line;
    echo form_label("Event Name", 'Event Name', $label);
    echo form_input($attr_event_name);

    /*location*/
    echo $break_line;
    echo form_label("Location:", 'location', $label);
    echo form_input($attr_event_location);

    echo $break_line;
    echo form_label("Date For the Event:", 'Event Date', $label);
    echo form_input($attr_event_date);

    echo $break_line;
    echo form_label("Upload Icon from The Event", 'location', $label);
    echo form_input($attr_event_icon);


    echo $break_line;
    echo form_label("Event Description:", 'Event Desc', $label);
    echo form_textarea($attr_event_desc);
    ?>

    <!--show js errors-->
    <div class="jsErrorOrSuccess">
        <div class="alert alert-success alert-dismissible" id="r_success"
             style="display: none;border-radius:0px;margin-top: 10px;"
             role="alert">
            <button type="button" class="close hidden" data-dismiss="alert"><span
                    aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <span id="r_message"></span>
        </div>

        <div class="alert alert-danger alert-dismissible" id="r_error"
             style="display: none;border-radius:0px;margin-top: 10px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span id="r_errormessage"></span>
        </div>

    </div> <!--end js error-->


    <div class="row">
        <br>

        <div class="col-md-8"></div>
        <div class="col-md-2">
            <?php
            echo form_button($attr_btn_save_event);
            ?>
        </div>
        <div class="col-md-2">
            <?php
            echo form_button($attr_btn_close);
            ?>
        </div>

    </div>
    <?php

    echo form_close() ?>


</div>
