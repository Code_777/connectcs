<div id="form_opportunity" class="form_mine">
    <!--show the errors here-->
    <!-- show errors-->
    <?php
    $attributes_company_in = array('class' => 'form-control input-sm',
        'id' => 'company_name',
        'placeholder' => 'eg Egerton University',
        'name' => 'company',
        'type' => 'text',
        'required' => ' ');

    $attributes_opp_level = array('class' => 'form-control input-sm',
        'id' => 'opp_level',
        'name' => 'opp_level',
        'placeholder' => 'eg Java Developer',
        'type' => 'text',
        'required' => ' ');

    $attributes_opp_description = array(
        'id' => 'opp_desc',
        'class' => 'form-control input-sm',
        'name' => 'opp_description',
        'type' => 'text',
        'placeholder' => 'eg The event is about',
        'required' => ' ',
        'cols' => '60',
        'rows' => '3');

    $attributes_date = array(
        'id' => 'opp_date',
        'class' => 'form-control input-sm',
        'name' => 'opp_date',
        'placeholder' => 'eg 2016-08-16',
        'type' => 'text',
        'place-holder' => 'eg The event is about',
        'required' => ' '
    );

    $attributes_location = array(
        'class' => 'form-control input-sm',
        'name' => 'location',
        'id' => 'location',
        'placeholder' => 'eg NAKURU KE',
        'type' => 'text',
        'place-holder' => 'eg The event is about',
        'required' => ' '
    );

    $btn_save = array(
        'id' => 'btn_save',
        'class' => 'btn custom_btn',
        'name' => 'button',
        'value' => 'true',
        'type' => 'submit',
        'content' => 'Save'
    );

    $break_line = "<br>";

    $btn_close = array(
        'class' => "btn custom_btn add_opp",
        'name' => 'button',
        'value' => 'true',
        'content' => 'Close',
    );

    $label = array(
        'style' => "font-family:'Roboto Thin';font-size: 1.2em;color: white"
    );

    $head = array(
        'style' => "font-family: 'Roboto Thin';font-size: 2em;color: white"
    );


    echo "<form id='form_opp'>";

    echo form_label("Opportunity Details", 'Opportunity details', $head);

    echo $break_line;
    echo form_label("Company or Organization:", 'company', $label);
    echo form_input($attributes_company_in);

    /*location*/
    echo $break_line;
    echo form_label("Location:", 'location', $label);
    echo form_input($attributes_location);

    echo $break_line;
    echo form_label("Date due:", 'opp_date', $label);
    echo form_input($attributes_date);

    echo $break_line;
    echo form_label("Opportunity Level:", 'opp_level', $label);
    echo form_input($attributes_opp_level);


    echo $break_line;
    echo form_label("Opportunity Description:", 'opp_description', $label);
    echo form_textarea($attributes_opp_description);
    ?>

    <div class="jsErrorOrSuccess">
        <div class="alert alert-success alert-dismissible" id="reset_success"
             style="display: none;border-radius:0px;margin-top: 10px;"
             role="alert">
            <button type="button" class="close hidden" data-dismiss="alert"><span
                    aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <span id="reset_message" style="font-family: 'Roboto Thin','Roboto Light';"></span>
        </div>

        <div class="alert alert-danger alert-dismissible" id="reset_error"
             style="display: none;border-radius:0px;margin-top: 10px; ">
            <button type="button" class="close hidden" data-dismiss="alert"><span
                    aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <span id="reset_errormessage" style="font-family: 'Roboto Light','Roboto Thin';"></span>
        </div>

    </div> <!--end js error-->


    <div class="row">
        <br>

        <div class="col-md-8"></div>
        <div class="col-md-2">
            <?php
            echo form_button($btn_save);
            ?>
        </div>
        <div class="col-md-2">
            <?php
            echo form_button($btn_close);
            ?>
        </div>

    </div>
    <?php

    echo form_close() ?>


</div>
