<!-- Modal event -->
<div id="myModal" class="modal custom fade " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p style="font-family: 'Roboto Thin';font-size: 2em">
                    Opportunity Details
                </p>

                <!--show errors-->
                <?php if ($this->session->flashdata('errors')): ?>
                    <?php echo $this->session->flashdata('errors'); ?>
                <?php endif; ?>
                <!--prepare the form and get data-->
                <?php $this->load->view('notice_board/forms/opportunity_form') ?>

            </div> <!--end modal body-->
        </div> <!--end modal content-->
    </div> <!--and modal dialog-->
</div>   <!--end modal-->

