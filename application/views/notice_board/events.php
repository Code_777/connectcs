<!--
event_name
event_logo
event_date-->


<?php


$view_to_display = "";
$event_counter = 1;
foreach ($events as $event) {

    $url_options = base_url() . "/assets/images/icons/options.png";
    $icon_url = base_url() . "/assets/images/uploads/" . $event->event_logo;
    $event_name = $event->event_name;
    $event_date = $event->event_date;
    $event_date = strtotime($event_date);
    $event_date = date('d M Y', $event_date);


    /*space*/
    echo "";

    /*icons*/
    echo "";

    /*date and company name*/
    echo "
<div class='row event' id='event$event_counter'>
    <div class='col-md-1'>
        <!--just some space-->
    </div>

<div class='col-md-1'> <!--icon-->
    <img src='$icon_url' style='background: 50% 50% no-repeat; width: 50px;height: 50px;border-radius: 50%'>
</div>


<div class='col-md-8'>
    <p class='event_holder_name'  id='event_name$event_counter'>
        $event_name
    </p>

    <p class='event_date' style='color: deeppink;'>
        $event_date
    </p>
</div><!-- title to event and event date -->
        
<div class='col-md-2 dropdown' style='padding-right:10px;'>
        <img class='dropdown-toggle' data-toggle='dropdown' src='$url_options' style='height:20px;float: right;margin-right: 10px;'>
            <ul class='dropdown-menu mine'>

            <li><a >Edit</a></li>
            <li><a class='delete_event' id='300$event_counter' onclick='delete_event(this.id)'>Delete</a></li>

            </ul>

</div>
</div>";
    
    //echo $view_an_event;
   // $view_to_display = $view_to_display . $view_an_event;

    $event_counter++;
} /*close foreach loop*/

//$view_to_display = str_replace("\r\n", '', $view_to_display);

?>