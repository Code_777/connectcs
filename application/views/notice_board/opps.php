<!--show all the opportunities available-->
<!--color generator-->

<?php
$var_counter = 0;
$var_id_generator = 0;
$color_palette = array('#00a651', '#ed1c24', '#448ccb', '#D81B60', '#42A5F5', '#43A047');

if (!(sizeof($opps) > 0)) {
    echo "<p class='danger'>No Opportunities posted Yet</p>";
}
foreach ($opps as $opportunity) {

    if ($var_counter > sizeof($color_palette) - 1) {
        $var_counter = $var_counter % (sizeof($color_palette) - 1);
    }

    $color = $color_palette[$var_counter];

    $briefcase_path = base_url() . "/assets/images/briefcase_small.png";
    $company = $opportunity->company;
    $opp_desc = $opportunity->opportunity_description;
    $date = $opportunity->opportunity_date_due;
    $date1 = strtotime($date);
    $date_due = date('d M Y', $date1);
    $opp_level = $opportunity->opportunity_level;
    $location = $opportunity->location;
    $url_options = base_url() . "/assets/images/icons/options.png";

    /*box with color*/
    echo "<div class='row opportunity' id='opportunity$var_id_generator'>
            <div class='col-md-2' style='margin-left: 35px'>
                    <div class='opportunity_box' style='background-color: $color'>
                        <img class='briefcase' src='$briefcase_path'>
                    </div>
                </div>";

    /*opp level */
    echo "<div class='col-md-6'>
                    <p class='opportunity_description'>
                    <span id='opp_level$var_id_generator'>
                        $opp_level
                    </span>
                    <br>
                        <span id='comp_name$var_id_generator' style='color: deeppink;'>
                            $company
                        </span><br>
                        $location
                    </p>
            </div>";


    echo "<div class='col-md-3'>
                    ";
                     $session_data=$this->session->userdata('user');
        if(isset($session_data['status']) | isset($session_data['level']))
        {
            echo"
                <div class='dropdown'>
                    <img class='dropdown-toggle' data-toggle='dropdown' src='$url_options' style='height:20px;float: right;margin-right: 10px;'>
                        <ul class='dropdown-menu mine'>
                            <li><a >Edit</a></li>
                            <li><a id='200$var_id_generator' onclick='delete_opp(this.id)' >Delete</a></li>
                        </ul>
                  </div>
                  ";
              }
              echo "
                  <p class='opportunity_readmore'>
                    <span id='opp_level$var_id_generator'>$opp_level</span>
                    <br>
                    $date_due<br>
                                        <span class='read_more' id='$var_id_generator' data-toggle='collapse' data-target='#demo$var_id_generator'>
                                            <a href='#'> Read more..</a>
                                    </span>
            </div>


</div>";

    /*read more */
    echo "<div class='row' >
            <div class='col-md-1'></div>
            <div id='demo$var_id_generator' class='col-md-10 collapse opp_desc_read' data-parent='#mother'  style='margin-left: 4px;'>$opp_desc</div>
            <div class='col-md-1'></div>
        </div>";


    $var_counter++;
    $var_id_generator++;
} /*end for loop*/

?>


<!--company name-->


<!--read more content-->
<!--end opportunity 1-->