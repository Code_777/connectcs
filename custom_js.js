/*start the date picker*/
$(function () {
    $("#opp_date").datepicker({dateFormat: 'yy-mm-dd'});
    $("#event_date").datepicker({dateFormat: 'yy-mm-dd'});
});


$(document).ready(function () {
    $(".add_opp").click(function () {
        $("#noticeboard").slideToggle("slow");
        $("#form_opportunity").slideToggle("slow");
    });
});

$(document).ready(function () {
    $(".add_event").click(function () {
        $("#noticeboard").slideToggle("slow");
        $("#form_event").slideToggle("slow");
    });
});
$(document).ready(function () {
    $(".show_lecs").click(function () {
        $("#noticeboard").slideToggle();
        $("#lec_d").slideToggle("slow");
    });
});


/*onclick for the form*/
function sendToDatabase() {
    var organization = $('#company_name').val();
    var opp_level = $('#opp_level').val();
    var opp_desc = $('#opp_desc ').val();
    var opp_date = $('#opp_date').val();
    var location = $('#location').val();

    if (organization != null &&
        opp_level != null &&
        opp_desc != null &&
        opp_date != null &&
        location != null) {
        console.log(opp_date + "\n" + opp_desc + "\n" + location + "\n"
            + opp_level);

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.status == 200 && xmlhttp.readyState == 4) {
                document.getElementById('txtHint').innerHTML = xmlhttp.responseText;
                console.log(xmlhttp.responseText);
            }
        };


        xmlhttp.open('GET', 'getHint.php?q=' + str, true);
        xmlhttp.send();
    }
}


/*working with search*/
function sendData() {
    $.ajax({
        url: 'index.php/custom_controller/addToDatabase',
        data: ({'opp_level': $('#opp_level').text()}),
        dataType: 'json',
        type: 'post',
        success: function (data) {
            response = jQuery.parseJSON(data);
            console.log(response);
        }
    });
}


/*hide the status fields*/
function clear_reset() {
    $('#reset_success').hide();
    $('#reset_error').hide();

    $('#r_success').hide();
    $('#r_error').hide();
}


/*send data to server*/
$(document).ready(function () {
    $('form#form_opp').submit(function (e) {
        e.preventDefault();


        var info = new FormData();
        info.append('company', $('#company_name').val());
        info.append('opp_level', $('#opp_level').val());
        info.append('opp_description', $('#opp_desc').val());
        info.append('location', $('#location').val());
        info.append('opp_date', $('#opp_date').val());

        $.ajax({
            url: 'index.php/custom_controller/addToDatabase',
            method: 'post',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            data: info,

            success: function (data) {
                if (data.success) {
                    clear_reset();
                    $('#reset_success').find('#reset_message').empty().append('<p>' + data.result + '</p>').addClass('text text-success');
                    $('#reset_success').show();
                } else {
                    clear_reset();
                    $('#reset_error').find('#reset_errormessage').empty().append(data.errors).addClass('text text-danger');
                    $('#reset_error').show();
                }
            }
        });
    });
});


/*get desc for opp*/
function get_desc(number_id) {
    var name = "#comp_name" + number_id;
    var level = '#opp_level' + number_id + '';
    var company_name = $(name).text();
    var opp_level = $(level).text();

    var info = new FormData();
    info.append('company_name', company_name);
    info.append('opp_level', opp_level);

    $.ajax({
        url: 'index.php/custom_controller/get_opp_desc',
        method: 'post',
        contentType: false,
        processData: false,
        cache: false,
        dataType: 'json',
        data: info,

        success: function (data) {
            if (data.success) {
                alert(data.desc);
            }
        }
    });
}


$(document).ready(function () {
    $('#search_field').keyup(function () {
        var search = $('#search_field').val();
        console.log(search);

        //$('.tab-content').slideToggle();
    });
});


/*delete event*/
function delete_event(number_id) {
    number_id = number_id - 3000;

    var name = "#event_name" + number_id;
    var event_name = $(name).text().trim();
    /*alert(event_name);*/

    var info = new FormData();
    info.append('event_name', event_name);

    $.ajax({
        url: 'index.php/custom_controller/delete_event',
        method: 'post',
        contentType: false,
        processData: false,
        cache: false,
        dataType: 'json',
        data: info,
        success: function (data) {
            if (data.success) {
                /* alert(data.delete_);*/
            }
        }
    });
}


/**/

/*create an event*/
$(document).ready(function () {

    $('form#form_e').submit(function (e) {
        e.preventDefault();


        var info = new FormData();
        info.append('event_name', $('#event_name').val());
        info.append('event_location', $('#event_location').val());
        info.append('event_desc', $('#event_desc').val());
        info.append('event_date', $('#event_date').val());
        info.append('event_icon', $('#event_icon')[0].files[0]);
        /*this line costs my day*/

        $.ajax({
            url: 'index.php/custom_controller/create_event',
            method: 'POST',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            data: info,

            success: function (data) {
                if (data.success) {
                    clear_reset();
                    $('#r_success').find('#r_message').empty().append('<p>' + data.msg + '</p>').addClass('text text-success');
                    $('#r_success').show();
                    refresh_divs();
                } else {
                    clear_reset();
                    $('#r_error').find('#r_errormessage').empty().append(data.msg).addClass('text text-danger');
                    $('#r_error').show();
                }
            }
        });

    });
});

/*function refresh*/
function refresh_divs() {
    $(document).ready(function () {
        $('#leftidv').load('index.php/notice_board/events');
    });
}

/*provide event data for editing*/



