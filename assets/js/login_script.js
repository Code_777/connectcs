/**
 * Created by James on 4/30/2016.
 */

$(document).ready(function () {
    $(".show_login").click(function () {
        $(".login_here").slideToggle("slow");
        $("#land_portion").slideToggle("up");
    });
});

 $(document).ready(function () {
        $('form#login_form').submit(function (e) {
            e.preventDefault();
            //alert("It worked");
            //var x=document.getElementById("id");
            var info = new FormData();
            info.append('username', $('#username').val());
            info.append('password', $('#password').val());
            //info.append('user_email', $('#user_email').val());
            $.ajax({
                url: 'login',
                method: 'POST',
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                data: info,
                success: function (data) {
                    if (data == "failed") {
                        //alert("It worked");
                        $('#failed').show();
                    } else {
                        //$('#failed').hide();
                        $('#failed').innerHTML = data;
                        window.location = data;
                    }
                }
            });
        });

    });
