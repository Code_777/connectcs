/*start the date picker*/
$(function () {
    $("#opp_date").datepicker({dateFormat: 'yy-mm-dd'});
    $("#event_date").datepicker({dateFormat: 'yy-mm-dd'});
});


$(document).ready(function () {
    $(".add_opp").click(function () {
        $("#hidenotice").slideToggle("slow");
        $("#form_opportunity").slideToggle("slow");
    });
});

$(document).ready(function () {
    $(".add_event").click(function () {
        $("#hidenotice").slideToggle("slow");
        $("#form_event").slideToggle("slow");
    });
});
$(document).ready(function () {
    $(".show_lecs").click(function () {
        $("#hidenotice").slideToggle();
        $("#lec_d").slideToggle("slow");
    });
});


/*onclick for the form*/
function sendToDatabase() {
    var organization = $('#company_name').val();
    var opp_level = $('#opp_level').val();
    var opp_desc = $('#opp_desc ').val();
    var opp_date = $('#opp_date').val();
    var location = $('#location').val();

    if (organization != null &&
        opp_level != null &&
        opp_desc != null &&
        opp_date != null &&
        location != null) {
        console.log(opp_date + "\n" + opp_desc + "\n" + location + "\n"
            + opp_level);

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.status == 200 && xmlhttp.readyState == 4) {
                document.getElementById('txtHint').innerHTML = xmlhttp.responseText;
                console.log(xmlhttp.responseText);
            }
        };


        xmlhttp.open('GET', 'getHint.php?q=' + str, true);
        xmlhttp.send();
    }
}


/*working with search*/
function sendData() {
    $.ajax({
        url: 'custom_controller/search',
        data: ({'opp_level': $('#opp_level').text()}),
        dataType: 'json',
        type: 'post',
        success: function (data) {
            response = jQuery.parseJSON(data);
            console.log(response);
        }
    });
}


/*hide the status fields*/
function clear_reset() {
    $('#reset_success').hide();
    $('#reset_error').hide();

    $('#r_success').hide();
    $('#r_error').hide();
}


/*send data to server*/
$(document).ready(function () {
    $('form#form_opp').submit(function (e) {
        e.preventDefault();


        var info = new FormData();
        info.append('company', $('#company_name').val());
        info.append('opp_level', $('#opp_level').val());
        info.append('opp_description', $('#opp_desc').val());
        info.append('location', $('#location').val());
        info.append('opp_date', $('#opp_date').val());

        $.ajax({
            url: 'custom_controller/addToDatabase',
            method: 'post',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            data: info,

            success: function (data) {
                if (data.success) {
                    clear_reset();
                    $('#reset_success').find('#reset_message').empty().append('<p>' + data.result + '</p>').addClass('text text-success');
                    $('#reset_success').show();
                } else {
                    clear_reset();
                    $('#reset_error').find('#reset_errormessage').empty().append(data.errors).addClass('text text-danger');
                    $('#reset_error').show();
                }
            }
        });
    });
});


/*get desc for opp*/
function get_desc(number_id) {
    var name = "#comp_name" + number_id;
    var level = '#opp_level' + number_id + '';
    var company_name = $(name).text();
    var opp_level = $(level).text();

    var info = new FormData();
    info.append('company_name', company_name);
    info.append('opp_level', opp_level);

    $.ajax({
        url: 'custom_controller/get_opp_desc',
        method: 'post',
        contentType: false,
        processData: false,
        cache: false,
        dataType: 'json',
        data: info,

        success: function (data) {
            if (data.success) {
                alert(data.desc);
            }
        }
    });
}

function cancel_search() {
    $("#content_shown").slideDown();
    $("#search_results").slideToggle();
}


function search() {

    if ($('#search_field').val() != "" || $('#search_field').val() == undefined) {
        var search = $('#search_field').val();

        var info = new FormData();
        info.append('search_word', search);


        /*search works from here*/
        $.ajax({
            url: 'custom_controller/search',
            method: 'post',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            data: info,

            success: function (data) {
                if (data.success) {
                    console.log(search);
                    console.log(data.matches_found);

                    $("#content_shown").slideUp();
                    $("#search_results").slideDown();

                }
            }
        });
    }

}


/*delete event*/
function delete_event(number_id) {
    number_id = number_id - 3000;

    var name = "#event_name" + number_id;
    var event_name = $(name).text().trim();
    //alert(name);

    var info = new FormData();
    info.append('event_name', event_name);
    var event_id = "#event" + number_id;

    $.ajax({
        url: 'custom_controller/delete_event',
        method: 'post',
        contentType: false,
        processData: false,
        cache: false,
        dataType: 'json',
        data: info,

        success: function (data) {
            if (data.success) {
                $(event_id).slideUp();
            }
        }
    });
}


/**/

/*create an event*/
$(document).ready(function () {
    $('form#form_e').submit(function (e) {
        e.preventDefault();

        var event_name = $('#event_name').val();
        var event_location = $('#event_location').val();
        var event_desc = $('#event_desc').val();
        var event_date = $('#event_date').val();
        var event_icon = $('#event_icon')[0].files[0];

        var info = new FormData();
        info.append('event_name', event_name);
        info.append('event_location', event_location);
        info.append('event_desc', event_desc);
        info.append('event_date', event_date);
        info.append('event_icon', event_icon);
        /*this line costs my day*/


        $.ajax({
            url: 'custom_controller/create_event',
            method: 'POST',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            data: info,

            success: function (data) {
                if (data.success) {
                    clear_reset();
                    $('#r_success').find('#r_message').empty().append('<p>' + data.msg + '</p>').addClass('text text-success');
                    $('#r_success').show();

                    var event_icon_r = $('#event_icon').val();
                    event_icon_r = "http://localhost/ConnFF/assets/images/uploads/" + event_icon_r.substring(12);
                    var toShow = add_event_html(event_name, event_icon_r, event_date, 12);

                    //alert(toShow);
                    $("#mother").append(toShow);

                } else {
                    clear_reset();
                    $('#r_error').find('#r_errormessage').empty().append(data.msg).addClass('text text-danger');
                    $('#r_error').show();
                }
            }
        });

    });
});

/*function refresh*/
function refresh_divs() {

    var info = new FormData();
    $.ajax({
        url: 'custom_controller/refresh_divs',
        method: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        dataType: 'json',
        data: info,

        success: function (data) {
            if (data.success) {
                alert(data.the_data_returned);

            } else {
                alert("on else");
            }
        }
    });

    alert("Helloo james");
}

/*provide event data for editing*/
var cw = $('.icon').width();
$('.icon').css({'height': cw + 'px'});

var c = $('.square_image').width();
$('.square_image').css({'height': c + 'px'});

/*adding an event*/
function add_event_html(event_name, event_logo, event_date, event_id) {
    var template = "<div class='row event' id='event1'>" +
        "<div class='col-md-1'>" +
        "</div>" +

        "<div class='col-md-1'>" +
        "<img src='" + event_logo + "' " +
        "style='background: 50% 50% no-repeat; width: 50px;height: 50px;border-radius: 50%'>" +
        "</div>" +
        "<div class= 'col-md-8' > " +
        "<p class= 'event_holder_name' id = 'event_name1' >" + event_name + "</p> " +
        "<p class= 'event_date' >" + event_date + "</p>" +
        "</div >" +

        "<div class = 'col-md-2 dropdown'style = 'padding-right:10px;' >" +
        "<img class= 'dropdown-toggle' data - toggle = 'dropdown' src = 'http://localhost/ConnFF/assets/images/icons/options.png' style= 'height:20px;float: right;margin-right: 10px;' > " +
        "<ul class= 'dropdown-menu mine' >" +
        "<li><a> Edit </a> </li>" +
        "<li><a class ='delete_event' id='" + event_id + "'onclick='delete_event(this.id)'>Delete</a></li>" +
        "</ul>" +
        "</div>" +

        "</div>";

    return template;
}


function delete_opp(opp_id) {
    var opportunity_id = opp_id - 2000;

    var opportunity_name_from_id = "#opp_level" + opportunity_id;
    var company_name_from_id = "#comp_name" + opportunity_id;

    var value = $(opportunity_name_from_id).text().trim();
    var value_company_name = $(company_name_from_id).text().trim();
    //alert(opportunity_name_from_id + "  " + value + "   " + value_company_name);


    var info = new FormData();
    info.append('company_name', value_company_name);
    info.append('opp_name', value);


    $.ajax({
        url: 'custom_controller/delete_opp',
        method: 'post',
        contentType: false,
        processData: false,
        cache: false,
        dataType: 'json',
        data: info,

        success: function (data) {
            if (data.success) {
                $("#opportunity" + opportunity_id).slideUp();
            }
        }
    });
}


/*
 number_id = number_id - 3000;

 var name = "#event_name" + number_id;
 var event_name = $(name).text().trim();
 //alert(name);

 var info = new FormData();
 info.append('event_name', event_name);
 var event_id = "#event" + number_id;

 $.ajax({
 url: 'custom_controller/delete_event',
 method: 'post',
 contentType: false,
 processData: false,
 cache: false,
 dataType: 'json',
 data: info,

 success: function (data) {
 if (data.success) {
 $(event_id).empty();
 }
 }
 });*/




