-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2016 at 02:53 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `connectcs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `level` varchar(10) NOT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`UserName`, `Password`, `level`) VALUES
('Code777', 'Code777', 'admin'),
('Gee', 'Gee', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE IF NOT EXISTS `alumni` (
  `al_id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `UserName` varchar(10) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Interests` text NOT NULL,
  `phone` int(10) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `level` varchar(7) NOT NULL,
  `work` varchar(20) NOT NULL,
  `avatar` varchar(50) NOT NULL,
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `att_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `descri` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `by` varchar(50) NOT NULL,
  `contacts` int(15) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `attachments`
--

INSERT INTO `attachments` (`att_id`, `name`, `location`, `descri`, `email`, `by`, `contacts`, `url`) VALUES
(1, 'UrbanKreative', 'Westland,Nairobi', 'Deals with web design and development for clients', 'info@UrbanKreative.com', 'Munene', 723788976, 'http://www.urbankreative.com'),
(2, 'NaiSoft', 'Eresearch towers,Nairobi', 'Deals with IT solutions inclusive of sofware and web solutions', 'info@webtribe.com', 'Munene', 723788371, 'http://www.webtribe.com'),
(3, 'Peak', 'Agip House,Nairobi', 'Deals with web design and development for clients', 'recruit@peakanddale.com', 'Never777', 713788076, 'http://www.peakanddale.com');

-- --------------------------------------------------------

--
-- Table structure for table `dislikes`
--

CREATE TABLE IF NOT EXISTS `dislikes` (
  `d_id` int(11) NOT NULL AUTO_INCREMENT,
  `disliked` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `UserName` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`d_id`),
  KEY `disliked` (`disliked`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `event_name` varchar(60) NOT NULL,
  `event_logo` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_desc` varchar(300) NOT NULL,
  `event_location` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_name`, `event_logo`, `event_date`, `event_desc`, `event_location`) VALUES
('dvhsfighrei', 'conn4.jpg', '2016-05-11', 'zvfwhgori', 'xvhsfioghoire');

-- --------------------------------------------------------

--
-- Table structure for table `lecturers`
--

CREATE TABLE IF NOT EXISTS `lecturers` (
  `lek_id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `UserName` varchar(10) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Interests` text NOT NULL,
  `phone` int(10) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `level` varchar(4) NOT NULL,
  `avatar` varchar(50) NOT NULL,
  PRIMARY KEY (`lek_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lecturers`
--

INSERT INTO `lecturers` (`lek_id`, `FirstName`, `LastName`, `UserName`, `Email`, `Interests`, `phone`, `Password`, `status`, `level`, `avatar`) VALUES
(1, 'hack', 'hack', 'hack777', 'hack@gamil.com', 'schaduchui', 1234567891, '405c559d2da04d56038ee247cf8f65bc', 1, 'lek', 'joy.png'),
(2, 'odiyo', 'odiyo', 'odiyo777', 'odiyo@gmail.com', 'skalchsaihdc', 1234567891, '5b79f5133053a3978430c52427925302', 1, 'lek', 'joy.png');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `liked` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `UserName` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`l_id`),
  KEY `liked` (`liked`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opp2`
--

CREATE TABLE IF NOT EXISTS `opp2` (
  `opp_id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(60) NOT NULL,
  `level` varchar(60) NOT NULL,
  `descri` text NOT NULL,
  `time` text NOT NULL,
  `location` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contacts` varchar(15) NOT NULL,
  `url` text NOT NULL,
  `by` varchar(50) NOT NULL,
  PRIMARY KEY (`opp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `opp2`
--

INSERT INTO `opp2` (`opp_id`, `company`, `level`, `descri`, `time`, `location`, `email`, `contacts`, `url`, `by`) VALUES
(1, 'Egerton University', 'Java Developer', 'This will be available for very short period of time', '2016-06-08', 'NAKURU KE', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Jackline'),
(2, 'James Company and Sons', 'Systems analyst', 'alskasjlajskjas', '2016-05-05', 'NAIROBI', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Ann'),
(3, 'Blue Website', 'Network Admin', 'al,slkasjlasljslaj', '2016-05-05', 'MOMBASA', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Laban'),
(4, 'Stark Industries', 'Java Developer', 'The developer have to be so  interested in Artificial Intelligence', '0000-00-00', 'Carlifornia Malibu', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Juma'),
(5, 'NaiSoft', 'Android Developer', 'Should be skilled enough for the tasks availed', '0000-00-00', 'NAKURU KE', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Fredrik'),
(6, 'Seven seas', 'Structural Engineer', 'Should be well skilled', '0000-00-00', 'Nakuru KE', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Steve'),
(7, 'Webtribe Company', 'C#  Developer', 'Should be well experienced', '0000-00-00', 'Carlifornia Malibu', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Kelvin'),
(8, 'Egerton University', 'The Creator', 'The day should be on a sunday', '2016-03-17', 'NAKURU KE', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Laban'),
(9, 'Justice League', 'Java Developer', 'Here we go dearie', '2016-07-07', 'Carlifornia Malibu', 'info@start.org', '+123647129', 'http://wwww.stark.org', 'Never777');

-- --------------------------------------------------------

--
-- Table structure for table `opportunity`
--

CREATE TABLE IF NOT EXISTS `opportunity` (
  `company` varchar(60) NOT NULL,
  `opportunity_level` varchar(60) NOT NULL,
  `opportunity_description` text NOT NULL,
  `opportunity_date_due` date NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opportunity`
--

INSERT INTO `opportunity` (`company`, `opportunity_level`, `opportunity_description`, `opportunity_date_due`, `location`) VALUES
('Egerton University', 'Java Developer', 'This will be available for very short period of time', '2016-06-08', 'NAKURU KE'),
('James Company and Sons', 'Systems analyst', 'alskasjlajskjas', '2016-05-05', 'NAIROBI'),
('Fiona', 'Network Admin', 'al,slkasjlasljslaj', '2016-05-05', 'MOMBASA'),
('Stark Industries', 'Java Developer', 'The developer have to be so  interested in Artificial Intelligence', '0000-00-00', 'Carlifornia Malibu'),
('Henry Jones and Sons', 'Android Developer', 'Should be skilled enough for the tasks availed', '0000-00-00', 'NAKURU KE'),
('James Gathu And Sons', 'Structural Engineer', 'Should be well skilled', '0000-00-00', 'Nakuru KE'),
('Peter''s Company', 'C#  Developer', 'Should be well experienced', '0000-00-00', 'Carlifornia Malibu'),
('Egerton University', 'The Creator', 'The day should be on a sunday', '2016-03-17', 'NAKURU KE'),
('Justice League', 'Java Developer', 'Here we go dearie', '2016-07-07', 'Carlifornia Malibu');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post` text NOT NULL,
  `time` date NOT NULL,
  `category` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `level` varchar(40) NOT NULL,
  `avatar` varchar(50) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`post_id`, `post`, `time`, `category`, `name`, `level`, `avatar`) VALUES
(6, 'how do i creatssas?', '2016-05-03', 'Web design', 'john777', 'null', 'joy.png');

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE IF NOT EXISTS `replies` (
  `r_key` int(11) NOT NULL AUTO_INCREMENT,
  `reply_id` int(11) NOT NULL,
  `reply` text NOT NULL,
  `time` date NOT NULL,
  `name` varchar(50) NOT NULL,
  `likes` int(9) DEFAULT NULL,
  `dislikes` int(9) DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`r_key`),
  KEY `reply_id` (`reply_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `replies`
--

INSERT INTO `replies` (`r_key`, `reply_id`, `reply`, `time`, `name`, `likes`, `dislikes`, `avatar`) VALUES
(1, 6, '...vbsdvgjnwfsmbsfn', '2016-05-03', 'Never777', 0, 0, 'conn41.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `replies2`
--

CREATE TABLE IF NOT EXISTS `replies2` (
  `r2_key` int(11) NOT NULL AUTO_INCREMENT,
  `reply2_id` int(11) NOT NULL,
  `reply2` text NOT NULL,
  `time` date NOT NULL,
  `nam` varchar(50) NOT NULL,
  `name2` varchar(50) NOT NULL,
  PRIMARY KEY (`r2_key`),
  KEY `reply2_id` (`reply2_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `std_id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `UserName` varchar(10) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `regno` varchar(12) NOT NULL,
  `Year` varchar(5) NOT NULL,
  `Interests` text NOT NULL,
  `Password` varchar(100) NOT NULL,
  `avatar` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`std_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`std_id`, `FirstName`, `LastName`, `UserName`, `Email`, `regno`, `Year`, `Interests`, `Password`, `avatar`, `status`) VALUES
(1, 'Grace', 'Muchiri', 'Grace', 'gracemuchiri200@gmail.com', 'S13/20763/13', '3', 'Web design and networking', 'grace777', 'grace.jpg', 0),
(2, 'Faith', 'Kadzo', 'Faith', 'faymlewa@gmail.com', 'S13/20771/13', '3', 'Web design and IOT', 'faithkadzo', 'Faith.jpg', 0),
(3, 'James', 'Gathu', 'Gathu', 'gathujamex@gmail.com', 'S13/20720/13', '3', 'Java, Android, web development and IOT', 'gathujamex', 'gathu.jpg', 0),
(4, 'Paul', 'Kuria', 'Munene', 'munenekuria777@gmail.com', 'S13/20782/13', '3', 'Penetration testing', 'forgot777', 'pmk.jpg', 1),
(5, 'Never', 'Never', 'Never777', 'never@gmail.com', 's13/207996/1', 'undef', 'hacking', 'b534e5240f3695080f6475be11f5de53', 'conn41.jpg', 0),
(6, 'Great', 'Great', 'Great777', 'great@gamil.com', 's13/20555/14', 'undef', 'dvjroghreohge', '9c0294aaa4b0eac3e45184be09cd3ba8', 'joy.png', 0),
(7, 'sweet', 'sweet', 'sweet777', 'sweet777@gmail.com', 's13/20789/56', 'undef', 'asfbsefhewi', '25e8e8456f395ed34610eaecc1eda050', 'joy.png', 0),
(8, 'dvhsrihgoirw', 'asfgweiufw4et9', 'avgeiuftsa', 'dvfgweiugfw7e@gamil.com', 'sehfweguf/af', 'undef', 'dvgsw7eft', 'a8b767bb9cf0938dc7f40603f33987e5', 'joy.png', 0),
(9, 'computer', 'computer', 'comp777', 'comp@gmail.com', 's13/456/56', '1', 'hacking', '7a6185b4140eb8868b36ffd2624d0f17', 'joy.png', 0),
(10, 'lek', 'lek', 'lek777', 'lek@gmail.com', 's13hsfiahd', '2', 'djfhwehfu', '200bf1d043553fba5f4bcf67ee093971', 'joy.png', 0),
(13, 'laptop', 'laptop', 'lap777', 'lap@gmail.com', 'ksdvsdivh', 'year3', 'ksdcjxbvcdjsbv', 'f976dc35d3b0923c1a90c0caf58af7e4', 'conn4.jpg', 0),
(14, 'joan', 'mary', 'joma', 'joma@gmail.com', 'S13/20798/13', '3', 'android programming', 'e10adc3949ba59abbe56e057f20f883e', 'joy.png', 0),
(15, 'JOhn', 'Peter', 'john777', 'john@gmail.com', 'S13/20758/13', '2', 'java programming', '3f0a8326a22765e09fe9264274317992', 'joy.png', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dislikes`
--
ALTER TABLE `dislikes`
  ADD CONSTRAINT `dislikes_ibfk_1` FOREIGN KEY (`disliked`) REFERENCES `replies` (`r_key`);

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`liked`) REFERENCES `replies` (`r_key`);

--
-- Constraints for table `replies`
--
ALTER TABLE `replies`
  ADD CONSTRAINT `replies_ibfk_1` FOREIGN KEY (`reply_id`) REFERENCES `post` (`post_id`);

--
-- Constraints for table `replies2`
--
ALTER TABLE `replies2`
  ADD CONSTRAINT `replies2_ibfk_1` FOREIGN KEY (`reply2_id`) REFERENCES `replies` (`r_key`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
